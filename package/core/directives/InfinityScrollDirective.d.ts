import { AfterViewInit, ElementRef, EventEmitter, NgZone } from '@angular/core';
import { Subject } from 'rxjs';
export declare class InfinityScrollDirective implements AfterViewInit {
    element: ElementRef;
    protected zone: NgZone;
    infinityScroll: EventEmitter<number>;
    scrollEvent: Subject<number>;
    constructor(element: ElementRef, zone: NgZone);
    ngAfterViewInit(): void;
}
