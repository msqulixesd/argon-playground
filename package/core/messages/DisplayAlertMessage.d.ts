import { MessageInterface } from '../interfaces/MessageInterface';
import { TemplateRef, ViewRef } from '@angular/core';
export declare class DisplayAlertMessage implements MessageInterface {
    message: string;
    bsContext: string;
    closeable: boolean;
    displayTime: number;
    static DEFAULT_DISPLAY_TIME: number;
    static type: string;
    type: string;
    viewRef: ViewRef;
    template?: TemplateRef<any>;
    templateContext: any;
    readonly isSimple: boolean;
    readonly shouldAutohide: boolean;
    constructor(message: string, bsContext?: string, closeable?: boolean, displayTime?: number);
    isMessageType(messageClass: any): boolean;
    setTemplate(template: TemplateRef<any>, context?: any): void;
}
