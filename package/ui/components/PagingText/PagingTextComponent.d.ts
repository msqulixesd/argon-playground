import { PagingInterface } from '../../interfaces/PagingInterface';
export declare class PagingTextComponent {
    paging: PagingInterface;
    readonly shouldDisplay: boolean;
}
