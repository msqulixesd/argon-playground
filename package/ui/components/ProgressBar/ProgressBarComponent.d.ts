import { ContextualService } from '../../services/ContextualService';
export declare class ProgressBarComponent {
    contextualService: ContextualService;
    static BASE_CLASS: string;
    showValue: boolean;
    striped: boolean;
    active: boolean;
    height: number;
    value: number;
    baseClass: boolean;
    role: string;
    ariaValueNow: string;
    ariaValueMin: string;
    ariaValueMax: string;
    hostStyle: number;
    readonly valueString: string;
    readonly style: any;
    private _value;
    constructor(contextualService: ContextualService);
}
