import { ToggleService } from '../../services/ToggleService';
import { ContextualService } from '../../services/ContextualService';
export declare class NavBarComponent {
    toggleService: ToggleService;
    contextualService: ContextualService;
    static BASE_CLASS: string;
    baseClass: boolean;
    readonly navBarClass: string;
    constructor(toggleService: ToggleService, contextualService: ContextualService);
}
