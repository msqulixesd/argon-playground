import { EventEmitter, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ContextualService } from '../../services/ContextualService';
export declare class PagingListSizeComponent implements OnInit, OnChanges, OnDestroy {
    contextualService: ContextualService;
    size: number;
    list: Array<number>;
    bsSize: string;
    onSizeChange: EventEmitter<number>;
    form: FormGroup;
    private changeSubscription;
    constructor(contextualService: ContextualService);
    ngOnChanges(changes: SimpleChanges): void;
    ngOnInit(): void;
    ngOnDestroy(): void;
}
