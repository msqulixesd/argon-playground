export declare class CardGroupComponent {
    deck: boolean;
    columns: boolean;
    group: boolean;
    isGroup: boolean;
    isDeck: boolean;
    isColumns: boolean;
}
