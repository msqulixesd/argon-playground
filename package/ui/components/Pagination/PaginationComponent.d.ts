import { EventEmitter, OnInit } from '@angular/core';
import { PageLinkInterface } from '../../interfaces/PageLinkInterface';
import { ContextualService } from '../../services/ContextualService';
export declare class PaginationComponent implements OnInit {
    contextualService: ContextualService;
    static PREV_TITLE: string;
    static NEXT_TITLE: string;
    static EMPTY_TITLE: string;
    static MAX_DISPLAY: number;
    static EDGE_COUNT: number;
    static MIN_COUNT: number;
    total: number;
    current: number;
    onPageSelect: EventEmitter<number>;
    readonly isVisible: boolean;
    pageLinks: Array<PageLinkInterface>;
    private _currentPage;
    private _total;
    constructor(contextualService: ContextualService);
    ngOnInit(): void;
    handlePageClick(page: PageLinkInterface): void;
    isActivePage(page: PageLinkInterface): boolean;
    private createPages(total);
    private createPageItem;
}
