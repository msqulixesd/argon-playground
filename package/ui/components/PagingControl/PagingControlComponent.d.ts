import { EventEmitter } from '@angular/core';
import { PagingControlChangeInterface } from '../../interfaces/PagingControlChangeInterface';
import { PagingInterface } from '../../interfaces/PagingInterface';
import { ContextualService } from '../../services/ContextualService';
export declare class PagingControlComponent {
    contextualService: ContextualService;
    paging: PagingInterface;
    bsSize: string;
    onChange: EventEmitter<PagingControlChangeInterface>;
    constructor(contextualService: ContextualService);
}
