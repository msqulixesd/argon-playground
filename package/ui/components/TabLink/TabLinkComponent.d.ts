import { EventEmitter } from '@angular/core';
import { TabModel } from '../../models/TabModel';
export declare class TabLinkComponent {
    model: TabModel;
    onClose: EventEmitter<TabModel>;
    closeTabHandler(event: any): void;
}
