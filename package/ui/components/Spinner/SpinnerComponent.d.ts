import { TemplateRef } from '@angular/core';
export declare class SpinnerComponent {
    visible: boolean;
    invisible: boolean;
    templateRef: TemplateRef<any>;
    state: boolean;
    show(): void;
    hide(): void;
}
