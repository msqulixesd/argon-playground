import { PagingModel } from './PagingModel';
import { PagingControlChangeInterface } from '../interfaces/PagingControlChangeInterface';
import { PagingInterface } from '../interfaces/PagingInterface';
/**
 * This is sample of PagingInterface implementation
 */
export declare class PagingQueryModel<T> implements PagingInterface {
    filter: T;
    pageSize: number;
    page: number;
    offset: number;
    orderBy: string;
    direction: 'asc' | 'desc';
    lastPagingResult?: PagingModel;
    constructor(filter: T);
    getPagesCount(): number;
    getStartRecord(): number;
    getEndRecord(): number;
    getTotalCount(): number;
    getPageSize(): number;
    getPage(): number;
    setOrder(orderBy: string, orderDirection?: 'asc' | 'desc'): void;
    setPageSize(size: number): void;
    applyChanges(changes: PagingControlChangeInterface): void;
    setPagingResult(model: PagingModel): void;
    updateFilter(filter: T): void;
    toJSON(): {
        pageSize: number;
        page: number;
        filter: T;
        orderBy: string;
        direction: "asc" | "desc";
    };
}
