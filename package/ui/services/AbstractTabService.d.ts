import { TabModel } from '../models/TabModel';
export declare abstract class AbstractTabService<T, K> {
    protected tabs: Array<T>;
    abstract find(params: K): T;
    abstract create(params: K): T;
    add(model: T): void;
    findOrCreate(params: K): T;
    remove(model: T): void;
    findByTab(tab: TabModel): T;
    removeByTab(tab: TabModel): void;
}
