/**
 * Modal dialog configuration interface
 */
export interface ModalDialogConfigInterface {
    large: boolean;
    small: boolean;
    customClass: string;
    backdropClickClose: boolean;
    centered: boolean;
}
