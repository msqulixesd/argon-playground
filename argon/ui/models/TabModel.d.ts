import { EmbeddedViewRef } from '@angular/core';
export declare class TabModel {
    title: string;
    closeable: boolean;
    changed: boolean;
    active: boolean;
    viewRef?: EmbeddedViewRef<any>;
    constructor(title?: string, closeable?: boolean);
}
