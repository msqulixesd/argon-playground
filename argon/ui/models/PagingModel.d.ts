export declare class PagingModel {
    page: number;
    pageSize: number;
    totalCount: number;
    offset: number;
}
