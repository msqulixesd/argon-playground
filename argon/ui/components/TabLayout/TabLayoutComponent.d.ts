import { TemplateRef, ViewContainerRef } from '@angular/core';
import { TabLayoutViewModel } from './TabLayoutViewModel';
import { TabModel } from '../../models/TabModel';
import { Subject } from 'rxjs';
export declare class TabLayoutComponent {
    content: ViewContainerRef;
    tabRemove: Subject<TabModel>;
    tabSelect: Subject<TabModel>;
    tabCreate: Subject<TabModel>;
    viewModel: TabLayoutViewModel;
    constructor();
    registerTab(tab: TabModel): void;
    removeTab(tab: TabModel): void;
    exist(tab: TabModel): boolean;
    deleteTab(tab: TabModel): void;
    createTab(tab: TabModel, template: TemplateRef<any>, context?: any): void;
    selectTab(tab: TabModel): void;
}
