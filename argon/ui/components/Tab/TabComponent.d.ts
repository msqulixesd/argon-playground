import { OnInit, ViewContainerRef, ViewRef } from '@angular/core';
import { TabLayoutComponent } from '../TabLayout/TabLayoutComponent';
import { TabModel } from '../../models/TabModel';
export declare class TabComponent implements OnInit {
    private tabLayout;
    dynamicContent: ViewContainerRef;
    title: string;
    selected: boolean;
    model: TabModel;
    readonly isActive: boolean;
    viewRef: ViewRef;
    constructor(tabLayout: TabLayoutComponent);
    ngOnInit(): void;
    updateTitle(title: string): void;
}
