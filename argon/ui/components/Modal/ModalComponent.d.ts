import { TemplateRef } from '@angular/core';
import { ModalService } from '../../services/ModalService';
import { ModalDialogConfigModel } from '../../models/ModalDialogConfigModel';
export declare class ModalComponent {
    private modalService;
    static createConfig(): ModalDialogConfigModel;
    modalTemplate: TemplateRef<any>;
    headerTemplate: TemplateRef<any>;
    bodyTemplate: TemplateRef<any>;
    footerTemplate: TemplateRef<any>;
    config: ModalDialogConfigModel;
    showHeader: boolean;
    showFooter: boolean;
    constructor(modalService: ModalService);
    show(context?: any): void;
    hide(): void;
}
