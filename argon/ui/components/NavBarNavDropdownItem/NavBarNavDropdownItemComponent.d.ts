import { DropdownComponent } from '../Dropdown/DropdownComponent';
export declare class NavBarNavDropdownItemComponent {
    dropdown: DropdownComponent;
    close(): void;
}
