import { ElementRef, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { IconService } from '../../services/IconService';
export declare class LayoutComponent implements OnInit {
    private iconService;
    private element;
    iconsSprite: ElementRef;
    documentClick: Subject<any>;
    constructor(iconService: IconService, element: ElementRef);
    ngOnInit(): void;
    closeHandler(event: any): void;
}
