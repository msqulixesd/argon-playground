import { ChangeDetectorRef, ElementRef } from '@angular/core';
import { ToggleService } from '../../services/ToggleService';
import { DropdownComponent } from '../Dropdown/DropdownComponent';
import { LayoutComponent } from '../Layout/LayoutComponent';
import { ContextualService } from '../../services/ContextualService';
export declare class DropdownMenuComponent extends DropdownComponent {
    contextualService: ContextualService;
    protected element: ElementRef;
    protected layout: LayoutComponent;
    protected ref: ChangeDetectorRef;
    toggleService: ToggleService;
    disabled: boolean;
    fullWidth: boolean;
    dropUp: boolean;
    constructor(contextualService: ContextualService, element: ElementRef, layout: LayoutComponent, ref: ChangeDetectorRef, toggleService: ToggleService);
}
