import { ElementRef, Renderer2 } from '@angular/core';
import { BsGroupComponent } from '../components/BsGroup/BsGroupComponent';
import { TextAlignEnumType } from '../types/TextAlignEnum';
export declare class TextAlignDirective {
    protected renderer2: Renderer2;
    protected elementRef: ElementRef;
    protected bsGroup: BsGroupComponent;
    protected currentClass: string;
    protected currentValue: string;
    arTextAlign: TextAlignEnumType;
    constructor(renderer2: Renderer2, elementRef: ElementRef, bsGroup: BsGroupComponent);
    protected decorate(nextDirection: string): void;
}
