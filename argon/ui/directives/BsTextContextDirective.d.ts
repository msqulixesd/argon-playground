import { ElementRef, Renderer2 } from '@angular/core';
import { BsGroupComponent } from '../components/BsGroup/BsGroupComponent';
import { BsTextContextType } from '../types/BsTextEnumType';
export declare class BsTextContextDirective {
    protected renderer2: Renderer2;
    protected elementRef: ElementRef;
    protected bsContextGroup: BsGroupComponent;
    static TEXT_PREFIX: string;
    static getClassName(context: BsTextContextType): string;
    private currentContext;
    private currentClass;
    arBsTextContext: BsTextContextType;
    constructor(renderer2: Renderer2, elementRef: ElementRef, bsContextGroup: BsGroupComponent);
    protected decorate(nextContext: BsTextContextType): void;
}
