import { ElementRef, Renderer2 } from '@angular/core';
import { LogService } from '../../core/services/LogService';
export declare class WidthDirective {
    protected renderer2: Renderer2;
    protected elementRef: ElementRef;
    private logService;
    static WIDTH_PREFIX: string;
    static ALLOWED_VALUES: string[];
    private currentWidth;
    private currentClass;
    arWidth: number;
    constructor(renderer2: Renderer2, elementRef: ElementRef, logService: LogService);
    protected decorate(nextWidth: number): void;
    protected checkWidthValue(value: any): void;
}
