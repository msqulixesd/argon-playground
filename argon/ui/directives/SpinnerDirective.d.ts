import { ComponentFactoryResolver, TemplateRef, ViewContainerRef } from '@angular/core';
export declare class SpinnerDirective {
    private templateRef;
    private viewContainer;
    arSpinner: boolean;
    private component;
    constructor(factoryResolver: ComponentFactoryResolver, templateRef: TemplateRef<any>, viewContainer: ViewContainerRef);
}
