import { BsContextDirective } from '../directives/BsContextDirective';
import { BsSizeDirective } from '../directives/BsSizeDirective';
export declare class ContextualService {
    protected bsContextDirective: BsContextDirective;
    protected bsSizeDirective: BsSizeDirective;
    baseClass: string;
    bsContext: any;
    bsContextPrefix: string;
    bsSizePrefix: string;
    bsSize: any;
    constructor(bsContextDirective: BsContextDirective, bsSizeDirective: BsSizeDirective);
    private setBaseClassName(contextualDirective, className);
}
