import { AnimationTriggerMetadata } from '@angular/animations';
export declare class ToggleService {
    static ACTIVE_STATE: string;
    static INACTIVE_STATE: string;
    static ANIMATION_HEIGHT: AnimationTriggerMetadata;
    static ANIMATION_OPACITY: AnimationTriggerMetadata;
    state: string;
    readonly boolState: boolean;
    toggle(): void;
    open(): void;
    close(): void;
}
