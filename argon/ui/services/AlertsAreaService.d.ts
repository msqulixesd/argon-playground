import { TemplateRef, ViewContainerRef } from '@angular/core';
import { DisplayAlertMessage } from '../../core/messages/DisplayAlertMessage';
export declare class AlertsAreaService {
    static MAX_ALERT_STACK_SIZE: number;
    private container;
    private template;
    registerContainer(container: ViewContainerRef): void;
    registerTemplate(template: TemplateRef<any>): void;
    addAlert: (message: DisplayAlertMessage) => void;
    removeAlert: (message: DisplayAlertMessage) => void;
    private removeOldestAlert();
    private removeAlertByViewRef(view);
    private createAutohideObserver(message);
}
