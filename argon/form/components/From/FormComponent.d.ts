import { OnInit } from '@angular/core';
import { FormService } from '../../services/FormService';
import { LogService } from '../../../core/services/LogService';
export declare class FormComponent implements OnInit {
    private formService;
    private logService;
    inline: boolean;
    id: string;
    bgContext: string;
    isInline: boolean;
    autocomplete: string;
    constructor(formService: FormService, logService: LogService);
    ngOnInit(): void;
}
