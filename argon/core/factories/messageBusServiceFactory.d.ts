import { MessageBusService } from '../services/MessageBusService';
import { LogService } from '../services/LogService';
export declare const messageBusServiceFactory: (logService: LogService) => MessageBusService;
