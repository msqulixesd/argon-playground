import { InfinityScrollDirective } from './InfinityScrollDirective';
import { OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { ScrollCollectionService } from '../services/ScrollCollectionService';
import { Subscription } from 'rxjs';
export declare class ScrollContainerDirective implements OnInit, OnDestroy {
    protected container: ViewContainerRef;
    protected template: TemplateRef<any>;
    protected scrollService: ScrollCollectionService;
    protected scrollDirective: InfinityScrollDirective;
    arScrollContainer: Array<any>;
    protected scrollSubscription: Subscription;
    constructor(container: ViewContainerRef, template: TemplateRef<any>, scrollService: ScrollCollectionService, scrollDirective: InfinityScrollDirective);
    ngOnInit(): void;
    ngOnDestroy(): void;
}
