import { OnDestroy, TemplateRef, ViewContainerRef } from '@angular/core';
import { DictionaryService } from '../services/DictionaryService';
export declare class DictionaryDirective implements OnDestroy {
    private container;
    private template;
    private dictionaryService;
    arDictionaryName: string;
    arDictionary: number;
    private _currentValue;
    private _currentDictionaryKey;
    private _currentDictionaryItem;
    private _dictionary;
    private _dictionarySubscription;
    constructor(container: ViewContainerRef, template: TemplateRef<any>, dictionaryService: DictionaryService);
    ngOnDestroy(): void;
    private subscribeToDictionary(value);
    private unsubscribeFromDictionary();
    private setDictionary;
    private updateView();
    private getDictionaryItem();
}
