import { BsContextType } from '../../argon/ui/types/BsContextEnumType';

export interface MenuItemInterface {

  title: string;

  url?: any;

  children?: Array<MenuItemInterface>;

  bsContext?: BsContextType;

  deprecated: boolean;

}
