import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CoreModule } from '../argon/core/CoreModule';
import { UiModule } from '../argon/ui/UiModule';
import { TablesModule } from '../argon/tables/TablesModule';
import { FormModule } from '../argon/form/FormModule';
import { ToolsModule } from '../argon/tools/ToolsModule';
import { router } from './routes';

import { MenuService } from './services/MenuService';
import { FakeDictionaryService } from './services/FakeDictionaryService';

import { HomePageComponent } from './components/pages/HomePage/HomePageComponent';
import { MainLayoutComponent } from './components/layouts/MainLayout/MainLayoutComponent';
import { IconsListComponent } from './components/parts/IconsList/IconsListComponent';
import { SourceCodeComponent } from './components/parts/SourceCode/SourceCodeComponent';
import { ExampleComponent } from './components/parts/Example/ExampleComponent';
import { RouterTreeComponent } from './components/parts/RouterTree/RouterTreeComponent';
import { PageLayoutComponent } from './components/layouts/PageLayout/PageLayoutComponent';
import { DeprecatedFeatureComponent } from './components/parts/DeprecatedFeature/DeprecatedFeatureComponent';
import { ContextDropdownComponent } from './components/parts/ContextDropdown/ContextDropdownComponent';
import { FormPageLayoutComponent } from './components/layouts/FormPageLayout/FormPageLayoutComponent';

import { CORE_MODULE } from './components/pages/CoreModule/components';
import { TOOLS_MODULE } from './components/pages/ToolsModule/components';
import { UI_MODULE } from './components/pages/UiModule/components';
import { TABLES_MODULE } from './components/pages/TablesModule/components';
import { FORMS_MODULE } from './components/pages/FormModule/components';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    CoreModule,
    UiModule,
    TablesModule,
    ToolsModule,
    FormModule,
    ReactiveFormsModule,
    HttpClientModule,
    router
  ],
  declarations: [
    HomePageComponent,
    MainLayoutComponent,
    PageLayoutComponent,
    FormPageLayoutComponent,
    IconsListComponent,
    SourceCodeComponent,
    ExampleComponent,
    RouterTreeComponent,
    DeprecatedFeatureComponent,
    ContextDropdownComponent,
    CORE_MODULE,
    UI_MODULE,
    TABLES_MODULE,
    FORMS_MODULE,
    TOOLS_MODULE,
  ],
  providers: [
    MenuService,
    FakeDictionaryService,
  ],
  bootstrap: [ MainLayoutComponent ]
})
export class AppModule { }
