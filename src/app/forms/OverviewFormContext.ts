import { FormContext } from '../../argon/form/models/FormContext';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { alphaDashValidator } from '../../argon/form/factories/syncValidatorFactory';

export class OverviewFormContext extends FormContext {

  static createForm() {
    const form = new FormGroup({
      text: new FormControl('Text Input', [ Validators.required ]),
      date: new FormControl(''),
      email: new FormControl('', [ Validators.email ]),
      password: new FormControl('', [ alphaDashValidator, Validators.minLength(6) ]),
      passwordConfirm: new FormControl('', [ alphaDashValidator ]),
      checkbox1: new FormControl(true, [Validators.required]),
      checkbox2: new FormControl(true),
      checkboxInline1: new FormControl(),
      checkboxInline2: new FormControl(true),
      checkboxInline3: new FormControl(true),
      radio1: new FormControl(null, [Validators.required]),
      select1: new FormControl(null, [Validators.required]),
      select2: new FormControl(),
      select3: new FormControl(),
      yesNoSelect: new FormControl(false, [Validators.required]),
      radioGroup: new FormControl(null, [Validators.required]),
      file: new FormControl(null, [Validators.required]),
      textarea: new FormControl(null, [Validators.required]),
      textarea2: new FormControl(),
      autocomplete: new FormControl(),
      autocompleteList1: new FormControl(),
      autocompleteList2: new FormControl(),
    });

    return new OverviewFormContext(form);
  }

  public get text(): AbstractControl { return this.form.get('text'); }
  public get date(): AbstractControl { return this.form.get('date'); }
  public get email(): AbstractControl { return this.form.get('email'); }
  public get password(): AbstractControl { return this.form.get('password'); }
  public get passwordConfirm(): AbstractControl { return this.form.get('passwordConfirm'); }
  public get checkbox1(): AbstractControl { return this.form.get('checkbox1'); }
  public get checkbox2(): AbstractControl { return this.form.get('checkbox2'); }
  public get checkboxInline1(): AbstractControl { return this.form.get('checkboxInline1'); }
  public get checkboxInline2(): AbstractControl { return this.form.get('checkboxInline2'); }
  public get checkboxInline3(): AbstractControl { return this.form.get('checkboxInline3'); }
  public get radio1(): AbstractControl { return this.form.get('radio1'); }
  public get select1(): AbstractControl { return this.form.get('select1'); }
  public get select2(): AbstractControl { return this.form.get('select2'); }
  public get select3(): AbstractControl { return this.form.get('select3'); }
  public get yesNoSelect(): AbstractControl { return this.form.get('yesNoSelect'); }
  public get radioGroup(): AbstractControl { return this.form.get('radioGroup'); }
  public get file(): AbstractControl { return this.form.get('file'); }
  public get textarea(): AbstractControl { return this.form.get('textarea'); }
  public get textarea2(): AbstractControl { return this.form.get('textarea2'); }
  public get autocomplete(): AbstractControl { return this.form.get('autocomplete'); }
  public get autocompleteList1(): AbstractControl { return this.form.get('autocompleteList1'); }
  public get autocompleteList2(): AbstractControl { return this.form.get('autocompleteList2'); }

  public get disabled(): boolean {
    return this.form.disabled;
  }
  public set disabled(val: boolean) {
    if (val) {
      this.form.disable();
    } else {
      this.form.enable();
    }
  }

  constructor(
    form: FormGroup
  ) {
    super(form);
    this.addFieldConfirmation('password', 'passwordConfirm', 'Password should be equals');
  }

  public validate(): boolean {
    return this.isValid();
  }

  public reset() {
    this.form.reset();
  }
}
