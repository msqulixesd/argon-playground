import { Component, OnInit } from '@angular/core';
import { map, range } from 'lodash';

@Component({
  selector: 'pg-large-table-page-component',
  templateUrl: './LargeTablePageComponent.html'
})
export class LargeTablePageComponent implements OnInit {

  public templateSource = require('./LargeTablePageComponent.html');

  public codeSource = ``;

  public rows: Array<any>;

  ngOnInit(): void {
    this.rows = map(range(1, 5000), this.createRow);
  }

  public createRow = (value: number) => {
    return {
      col1: `Row ${value}`,
      col2: 'Column 2',
      col3: 'Column 3'
    };
  }
}
