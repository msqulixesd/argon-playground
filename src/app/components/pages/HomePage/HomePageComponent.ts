import { Component } from '@angular/core';

@Component({
  selector: 'pg-home-page',
  templateUrl: 'HomePageComponent.html'
})
export class HomePageComponent {

  public iconsColor = 'secondary';

  public iconsSize = 128;

}
