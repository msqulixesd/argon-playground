import { AutocompleteDictionaryPageComponent } from './AutocompleteDictionaryPage/AutocompleteDictionaryPageComponent';
import { AutocompleteListPageComponent } from './AutocompleteListPage/AutocompleteListPageComponent';
import { AutocompletePageComponent } from './AutocompletePage/AutocompletePageComponent';
import { TextareaPageComponent } from './TextareaPage/TextareaPageComponent';
import { InputGroupPageComponent } from './InputGroupPage/InputGroupPageComponent';
import { ArFormPageComponent } from './ArFormPage/ArFormPageComponent';
import { YesNoSelectPageComponent } from './YesNoSelectPage/YesNoSelectPageComponent';
import { SelectMultiplePageComponent } from './SelectMultiplePage/SelectMultiplePageComponent';
import { SelectPageComponent } from './SelectPage/SelectPageComponent';
import { SelectOptionInterfacePageComponent } from './SelectOptionInterfacePage/SelectOptionInterfacePageComponent';
import { RadioGroupPageComponent } from './RadioGroupPage/RadioGroupPageComponent';
import { RadioPageComponent } from './RadioPage/RadioPageComponent';
import { FilePageComponent } from './FilePage/FilePageComponent';
import { CheckboxPageComponent } from './CheckboxPage/CheckboxPageComponent';
import { InputPageComponent } from './InputPage/InputPageComponent';
import { FormOverviewComponent } from './FormOverview/FormOverviewComponent';
import { FormPageComponent } from './FormPage/FormPageComponent';

export const FORMS_MODULE = [
  FormPageComponent,
  FormOverviewComponent,
  InputPageComponent,
  CheckboxPageComponent,
  FilePageComponent,
  RadioPageComponent,
  RadioGroupPageComponent,
  SelectOptionInterfacePageComponent,
  SelectPageComponent,
  SelectMultiplePageComponent,
  YesNoSelectPageComponent,
  ArFormPageComponent,
  InputGroupPageComponent,
  TextareaPageComponent,
  AutocompletePageComponent,
  AutocompleteListPageComponent,
  AutocompleteDictionaryPageComponent,
];
