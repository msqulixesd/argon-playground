import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'pg-textarea-page-component',
  templateUrl: './TextareaPageComponent.html'
})
export class TextareaPageComponent {

  public templateSource = require('./TextareaPageComponent.html');

  public codeSource = `
export class TextareaComponent extends InputComponent {

  @Input() placeholder = '';

  @Input() control: AbstractControl;

  @Input() name: string;

  @Input() resize: boolean;

  @Input() size  = 4;

  @Input() multiLine: boolean;
  
}  
`;

  public control = new FormControl(null, [Validators.required]);

}
