import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { DictionaryService } from '../../../../../argon/core/services/DictionaryService';
import { FakeDictionaryService } from '../../../../services/FakeDictionaryService';

@Component({
  selector: 'pg-autocomplete-dictionary-page-component',
  templateUrl: './AutocompleteDictionaryPageComponent.html'
})
export class AutocompleteDictionaryPageComponent implements OnInit {

  public templateSource = require('./AutocompleteDictionaryPageComponent.html');

  public codeSource = `
export class AutocompleteDictionaryComponent extends AutocompleteListComponent {

  @Input() placeholder = '';

  @Input() control: AbstractControl;

  @Input() name: string;

  @Input() noResultText = 'Nothing found';

  @Input() keyUpDelay = 500;

  @Input() dictionary: string;

  @Input() optionTemplate: TemplateRef<any>;

  @Input() defaultOption: SelectOptionInterface;

  @Input() dropUp: boolean;
  
}
`;

  public control = new FormControl(null, [Validators.required]);

  public dictionaryKey: 'FAKE_DICTIONARY';

  constructor(
    private dictionaryService: DictionaryService,
    private fakeDictionaryService: FakeDictionaryService
  ) { }

  ngOnInit(): void {
    this.dictionaryService.add(this.dictionaryKey, this.fakeDictionaryService.createDictionaryObservable(500));
  }
}
