import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'pg-checkbox-page-component',
  templateUrl: './CheckboxPageComponent.html'
})
export class CheckboxPageComponent {

  public templateSource = require('./CheckboxPageComponent.html');

  public codeSource = `
export class CheckboxComponent extends InputComponent {

  @Input() inline: boolean;

  @Input() control: AbstractControl;

  @Input() name: string;
  
}

  `;

  public control = new FormControl(null, [Validators.required]);

}
