import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { SelectOptionInterface } from '../../../../../argon/form/interfaces/SelectOptionInterface';

@Component({
  selector: 'pg-radio-group-page-component',
  templateUrl: './RadioGroupPageComponent.html'
})
export class RadioGroupPageComponent {

  public templateSource = require('./RadioGroupPageComponent.html');

  public codeSource = `
export class RadioGroupComponent extends SelectComponent {

  @Input() options: Array<SelectOptionInterface> = [];

  @Input() list: Array<string>;

  @Input() inline: boolean;

  @Input() control: AbstractControl;

  @Input() name: string;

}
  
export interface SelectOptionInterface {

  label?: string;

  value: any;

  disabled?: boolean;

  extendedData?: any;
}
`;

  public control = new FormControl(null, [Validators.required]);

  public options: Array<SelectOptionInterface> = [
    { label: 'Radio 1', value: 1 },
    { label: 'Radio 2', value: 2 },
    { label: 'Radio 3', value: 3, disabled: true },
    { label: 'Radio 4', value: 4 },
  ];

}
