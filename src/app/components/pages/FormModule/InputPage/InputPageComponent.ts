import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'pg-input-page-component',
  templateUrl: './InputPageComponent.html'
})
export class InputPageComponent {

  public templateSource = require('./InputPageComponent.html');

  public codeSource = `

export class InputComponent {

  @Input() type = 'text';

  @Input() placeholder = '';

  @Input() control: AbstractControl;

  @Input() name: string;
  
}

`;

  public control = new FormControl(null, [Validators.required]);

  public form = new FormGroup({
    control: this.control
  });

}
