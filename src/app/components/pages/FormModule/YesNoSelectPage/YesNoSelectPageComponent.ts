import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'pg-yes-no-select-page-component',
  templateUrl: './YesNoSelectPageComponent.html'
})
export class YesNoSelectPageComponent {

  public templateSource = require('./YesNoSelectPageComponent.html');

  public codeSource = `
export class YesNoSelectComponent extends SelectComponent {

  @Input() control: AbstractControl;

  @Input() placeholder = '';

  @Input() size = 1;

  @Input() name: string;

  @Input() yesLabel = 'Yes';

  @Input() noLabel = 'No';
  
}
  `;

  public control = new FormControl(null, [Validators.required]);

}
