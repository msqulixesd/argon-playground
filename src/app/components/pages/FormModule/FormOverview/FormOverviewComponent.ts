import { Component } from '@angular/core';
import { map, range } from 'lodash';
import { OverviewFormContext } from '../../../../forms/OverviewFormContext';
import { Observable, of } from 'rxjs';
import { SelectOptionInterface } from '../../../../../argon/form/interfaces/SelectOptionInterface';

@Component({
  selector: 'pg-form-overview-component',
  templateUrl: './FormOverviewComponent.html'
})
export class FormOverviewComponent {

  public templateSource = require('./FormOverviewComponent.html');

  public codeSource = require('!!raw-loader!../../../../forms/OverviewFormContext.ts');

  public overViewFormContext = OverviewFormContext.createForm();

  public size = 'sm';

  public context = 'primary';

  public bgContext = '';

  public largeList = of(
    map(
      range(1, 1000),
      (value: number) => ({ value, label: `Option ${value}`} as SelectOptionInterface)
    )
  );

  public autocompleteSearch(query: string): Observable<Array<SelectOptionInterface>> {
    return of([
      { value: 1, label: 'Option 1' },
      { value: 2, label: 'Option 2' },
      { value: 3, label: 'Option 3' },
      { value: 4, label: 'Option 4' },
      { value: 5, label: 'Option 5' }
    ]);
  }
}
