import { Component } from '@angular/core';
import { map, range } from 'lodash';
import { FormControl, Validators } from '@angular/forms';
import { SelectOptionInterface } from '../../../../../argon/form/interfaces/SelectOptionInterface';
import { of } from 'rxjs';

@Component({
  selector: 'pg-autocomplete-list-page-component',
  templateUrl: './AutocompleteListPageComponent.html'
})
export class AutocompleteListPageComponent {

  public templateSource = require('./AutocompleteListPageComponent.html');

  public codeSource = `
export class AutocompleteListComponent extends AutocompleteComponent {

  @Input() placeholder = '';

  @Input() control: AbstractControl;

  @Input() name: string;

  @Input() noResultText = 'Nothing found';

  @Input() keyUpDelay = 500;

  @Input() options: Array<SelectOptionInterface> | Observable<Array<SelectOptionInterface>>

  @Input() optionTemplate: TemplateRef<any>;

  @Input() defaultOption: SelectOptionInterface;

  @Input() dropUp: boolean;
  
} 

export interface SelectOptionInterface {

  label?: string;

  value: any;

  disabled?: boolean;

  extendedData?: any;
}  
`;

  public control = new FormControl(null, [Validators.required]);

  public options = of(
    map(
      range(1, 1000),
      (value: number) => ({ value, label: `Option ${value}`} as SelectOptionInterface)
    )
  );
}
