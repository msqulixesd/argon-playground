import { Component, NgZone } from '@angular/core';
import { map, range } from 'lodash';

@Component({
  selector: 'pg-scroll-directive-page-component',
  templateUrl: './ScrollDirectivePageComponent.html'
})
export class ScrollDirectivePageComponent {

  public templateSource = require('./ScrollDirectivePageComponent.html');

  public infinityScrollImplementation = require('!!raw-loader!./InfinityScroll.info');

  public scrollContainerImplementation = require('!!raw-loader!./ScrollContainer.info');

  public codeSource = `
export class InfinityScrollDirective {

  @Output() infinityScroll = new EventEmitter<number>();
  
}


export class ScrollContainerDirective {

  @Input() arScrollContainer: Array<any>;
  
}
`;

  public scrollRatio: number;

  public collection = map(range(0, 500), (item: number) => ({ index: item, anyOtherData: 'Data sample' }));

  constructor(
    private ngZone: NgZone
  ) { }

  public setScrollRatio(ratio: number) {
    this.ngZone.run(() => { this.scrollRatio = ratio; });
  }

}
