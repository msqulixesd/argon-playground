import { Component } from '@angular/core';

@Component({
  selector: 'pg-component-subscriptions-service-component',
  templateUrl: './ComponentSubscriptionsServiceComponent.html'
})
export class ComponentSubscriptionsServiceComponent {

  public templateSource = require('./ComponentSubscriptionsServiceComponent.html');

  public codeSource = require('!!raw-loader!./ComponentSubscriptionsService.info');

  public example = require('!!raw-loader!./ComponentSubscriptionsServiceComponent.info');

}
