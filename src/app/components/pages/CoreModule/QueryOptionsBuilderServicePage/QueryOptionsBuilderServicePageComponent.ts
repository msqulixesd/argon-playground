import { Component } from '@angular/core';
import { QueryOptionsBuilderService } from '../../../../../argon/core/services/QueryOptionsBuilderService';

@Component({
  selector: 'pg-query-options-builder-service-page-component',
  templateUrl: './QueryOptionsBuilderServicePageComponent.html'
})
export class QueryOptionsBuilderServicePageComponent {

  public templateSource = require('./QueryOptionsBuilderServicePageComponent.html');

  public codeSource = require('!!raw-loader!./QueryOptionsBuilderServicePage.info');

  public sampleObject = { a: 'b', c: ['d', 'e=f'], f: [['g'], ['h']] };

  constructor(
    public queryOptionsBuilder: QueryOptionsBuilderService
  ) { }

}
