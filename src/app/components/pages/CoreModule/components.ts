import { ComponentSubscriptionsServiceComponent } from './ComponentSubscriptionsServicePage/ComponentSubscriptionsServiceComponent';
import { QueryOptionsBuilderServicePageComponent } from './QueryOptionsBuilderServicePage/QueryOptionsBuilderServicePageComponent';
import { ScrollDirectivePageComponent } from './ScrollDirectivePage/ScrollDirectivePageComponent';
import { LogServicePageComponent } from './LogServicePage/LogServicePageComponent';
import { MessageBusServicePageComponent } from './MessageBusServicePage/MessageBusServicePageComponent';
import { DictionaryDirectivePageComponent } from './DictionaryDirectivePage/DictionaryDirectivePageComponent';
import { PipesPageComponent } from './PipesPage/PipesPageComponent';
import { DictionaryServicePageComponent } from './DictionaryServicePage/DictionaryServicePageComponent';
import { CorePageComponent } from './CorePage/CorePageComponent';

export const CORE_MODULE = [
  // Core Module
  CorePageComponent,
  DictionaryServicePageComponent,
  PipesPageComponent,
  DictionaryDirectivePageComponent,
  MessageBusServicePageComponent,
  LogServicePageComponent,
  ScrollDirectivePageComponent,
  QueryOptionsBuilderServicePageComponent,
  ComponentSubscriptionsServiceComponent,
];
