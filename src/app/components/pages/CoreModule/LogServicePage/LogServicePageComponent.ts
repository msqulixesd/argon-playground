import { Component } from '@angular/core';

@Component({
  selector: 'pg-log-service-page-component',
  templateUrl: './LogServicePageComponent.html'
})
export class LogServicePageComponent {

  public templateSource = require('./LogServicePageComponent.html');

  public codeSource = `
export class LogService {

  log(...args): void;

  warn(...args): void;

}

`;

}
