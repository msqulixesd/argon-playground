import { Component, OnInit } from '@angular/core';
import { DictionaryService } from '../../../../../argon/core/services/DictionaryService';
import { Observable, of } from 'rxjs/index';
import { DictionaryItemInterface } from '../../../../../argon/core/interfaces/DictionaryItemInterface';
import { FakeDictionaryService } from '../../../../services/FakeDictionaryService';

@Component({
  selector: 'pg-dictionary-service-page-component',
  templateUrl: './DictionaryServicePageComponent.html'
})
export class DictionaryServicePageComponent implements OnInit {

  public templateSource = require('./DictionaryServicePageComponent.html');

  public codeSource = require('!!raw-loader!./DictionaryService.info');

  public implementation = require('!!raw-loader!./ComponentImplementation.info');

  public dictionaryObserver: Observable<Array<DictionaryItemInterface>> = of([]);

  public dictionary: Array<DictionaryItemInterface> = [];

  constructor(
    public dictionaryService: DictionaryService,
    private fakeDictionaryService: FakeDictionaryService
  ) { }

  ngOnInit(): void {
    this.setDictionary1();
    this.dictionaryObserver = this.dictionaryService.get('TEST');

    this.dictionaryObserver.subscribe((data: Array<DictionaryItemInterface>) => {
      this.dictionary = data;
    });
  }

  public setDictionary1() {
    this.dictionaryService.add('TEST', this.fakeDictionaryService.createDictionaryArray(7));
  }

  public setDictionary2() {
    this.dictionaryService.add('TEST', this.fakeDictionaryService.createDictionaryObservable(7));
  }
}
