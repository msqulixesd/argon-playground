import { Component } from '@angular/core';

@Component({
  selector: 'pg-message-bus-service-page-component',
  templateUrl: './MessageBusServicePageComponent.html'
})
export class MessageBusServicePageComponent {

  public templateSource = require('./MessageBusServicePageComponent.html');

  public implementation = require('!!raw-loader!./MessageBusService.info');

  public codeSource = `
export class MessageBusService {

  public of<T>(messageClass: any): Observable<T>;

  public send(message: MessageInterface): void;
  
}

export interface MessageInterface {

  type: string;
  
  isMessageType(messageClass: any): boolean;

}
`;

}
