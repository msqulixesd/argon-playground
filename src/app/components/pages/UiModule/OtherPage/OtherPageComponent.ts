import { Component } from '@angular/core';
import { BsSizeArray } from '../../../../../argon/ui/types/BsSizeEnumType';
import { BsContextArray } from '../../../../../argon/ui/types/BsContextEnumType';

@Component({
  selector: 'pg-other-page-component',
  templateUrl: './OtherPageComponent.html'
})
export class OtherPageComponent {

  public templateSource = require('./OtherPageComponent.html');

  public codeSource = `
export class BlockComponent {

  @Input() title: string;

}

export class FigureComponent {

  @Input() captionAlign: string;

}

export class LoremIpsumComponent {
  
  @Input() length: number;

}

export class HelpBlockComponent {

  @Input() mini: boolean;

}

export class SwitchComponent {

  @Input() active: boolean;

  @Input() disabled: boolean;

  @Output() toggle = new EventEmitter<void>();
  
}
`;

  public bsSize = BsSizeArray;

  public bsContext = BsContextArray;

  public switchState: boolean;
}
