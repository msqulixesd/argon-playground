import { GridPageComponent } from './GridPage/GridPageComponent';
import { SpinnerPageComponent } from './SpinnerPage/SpinnerPageComponent';
import { ProgressBarPageComponent } from './ProgressBarPage/ProgressBarPageComponent';
import { CardPageComponent } from './CardPage/CardPageComponent';
import { DirectivesPageComponent } from './DirectivesPage/DirectivesPageComponent';
import { PanelPageComponent } from './PanelPage/PanelPageComponent';
import { ModalPageComponent } from './ModalPage/ModalPageComponent';
import { LinksPageComponent } from './LinksPage/LinksPageComponent';
import { PagingPageComponent } from './PagingPage/PagingPageComponent';
import { OtherPageComponent } from './OtherPage/OtherPageComponent';
import { NavBarPageComponent } from './NavBarPage/NavBarPageComponent';
import { BsGroupPageComponent } from './BsGroupPage/BsGroupPageComponent';
import { TabPageComponent } from './TabPage/TabPageComponent';
import { DropdownPageComponent } from './DropdownPage/DropdownPageComponent';
import { ButtonsPageComponent } from './ButtonsPage/ButtonsPageComponent';
import { IconsPageComponent } from './IconsPage/IconsPageComponent';
import { UiPageComponent } from './UiPage/UiPageComponent';
import { AlertPageComponent } from './AlertPage/AlertPageComponent';

export const UI_MODULE = [
  UiPageComponent,
  IconsPageComponent,
  ButtonsPageComponent,
  DropdownPageComponent,
  TabPageComponent,
  BsGroupPageComponent,
  NavBarPageComponent,
  OtherPageComponent,
  AlertPageComponent,
  PagingPageComponent,
  LinksPageComponent,
  ModalPageComponent,
  PanelPageComponent,
  DirectivesPageComponent,
  CardPageComponent,
  ProgressBarPageComponent,
  SpinnerPageComponent,
  GridPageComponent,
];
