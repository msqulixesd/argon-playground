import { Component } from '@angular/core';
import { BsSizeArray } from '../../../../../argon/ui/types/BsSizeEnumType';
import { BsContextArray } from '../../../../../argon/ui/types/BsContextEnumType';

@Component({
  selector: 'pg-directives-page-component',
  templateUrl: './DirectivesPageComponent.html'
})
export class DirectivesPageComponent {

  public templateSource = require('./DirectivesPageComponent.html');

  public codeSource = ``;

  public bsSize = BsSizeArray;

  public bsContext = BsContextArray;

}
