import { Component } from '@angular/core';
import { BsSizeArray } from '../../../../../argon/ui/types/BsSizeEnumType';
import { BsContextArray } from '../../../../../argon/ui/types/BsContextEnumType';

@Component({
  selector: 'pg-progress-bar-page-component',
  templateUrl: './ProgressBarPageComponent.html'
})
export class ProgressBarPageComponent {

  public templateSource = require('./ProgressBarPageComponent.html');

  public codeSource = `
export class ProgressBarComponent {

  @Input() showValue = false;

  @Input() striped = false;

  @Input() active = false;

  @Input() value = 0; 
  
  @Input() height: number;
  
}
`;

  public bsSize = BsSizeArray;

  public bsContext = BsContextArray;

}
