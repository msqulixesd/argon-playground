import { Component, OnInit } from '@angular/core';
import { BsSizeArray } from '../../../../../argon/ui/types/BsSizeEnumType';
import { BsContextArray } from '../../../../../argon/ui/types/BsContextEnumType';
import { ModalDialogConfigModel } from '../../../../../argon/ui/models/ModalDialogConfigModel';

@Component({
  selector: 'pg-modal-page-component',
  templateUrl: './ModalPageComponent.html'
})
export class ModalPageComponent implements OnInit {

  public templateSource = require('./ModalPageComponent.html');

  public codeSource = `
export class ModalComponent {

  @Input() headerTemplate: TemplateRef<any>;

  @Input() bodyTemplate: TemplateRef<any>;

  @Input() footerTemplate: TemplateRef<any>;

  @Input() config: ModalDialogConfigModel = ModalComponent.createConfig();

  @Input() showHeader = true;

  @Input() showFooter = true;
  
}  

export class ModalDialogConfigModel implements ModalDialogConfigInterface {

}

export interface ModalDialogConfigInterface {

  large: boolean;

  small: boolean;

  customClass: string;

  backdropClickClose: boolean;

  centered: boolean;

}

export class ModalHeaderComponent {

}
`;

  public bsSize = BsSizeArray;

  public bsContext = BsContextArray;

  public modalConfig: ModalDialogConfigModel;

  ngOnInit(): void {
    this.modalConfig = new ModalDialogConfigModel();
    this.modalConfig.large = true;
    this.modalConfig.backdropClickClose = false;
    this.modalConfig.centered = true;
  }
}
