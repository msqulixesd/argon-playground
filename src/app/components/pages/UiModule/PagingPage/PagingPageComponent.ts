import { Component, OnInit } from '@angular/core';
import { BsSizeArray } from '../../../../../argon/ui/types/BsSizeEnumType';
import { BsContextArray } from '../../../../../argon/ui/types/BsContextEnumType';
import { PagingQueryModel } from '../../../../../argon/ui/models/PagingQueryModel';
import { PagingModel } from '../../../../../argon/ui/models/PagingModel';
import { PagingControlChangeInterface } from '../../../../../argon/ui/interfaces/PagingControlChangeInterface';

@Component({
  selector: 'pg-paging-page-component',
  templateUrl: './PagingPageComponent.html'
})
export class PagingPageComponent implements OnInit {

  public templateSource = require('./PagingPageComponent.html');

  public codeSource = `
export class PagingListSizeComponent {

  @Input() size: number;

  @Input() list: Array<number> = [10, 20, 50, 100];

  @Input() bsSize = BsSizeEnum.SM;

  @Output() onSizeChange = new EventEmitter<number>();
  
}

export class PaginationComponent implements OnInit {

  @Input() bsSize = BsSizeEnum.SM;

  @Input() total: number;

  @Input() current: number;

  @Output() onPageSelect = new EventEmitter<number>();
  
}

export class PagingControlComponent {

  @Input() paging: PagingInterface;

  @Input() bsSize = BsSizeEnum.SM;

  @Output() onChange = new EventEmitter<PagingControlChangeInterface>();

}

export interface PagingInterface {
  
  getPagesCount(): number;
  
  getStartRecord(): number;
  
  getEndRecord(): number;
  
  getTotalCount(): number;
  
  getPageSize(): number;
  
  getPage(): number;
  
  setOrder(orderBy: string, orderDirection: 'asc' | 'desc'): void;
  
  setPageSize(size: number): void;
  
  applyChanges(changes: PagingControlChangeInterface): void;
  
}

export interface PagingControlChangeInterface {
  
  pageSize: number;
  
  page: number;
  
}



/**
 * This is sample of PagingInterface implementation
 */
export class PagingQueryModel<T> implements PagingInterface {

  public pageSize = 50;

  public page = 1;

  public offset = 0;

  public orderBy: string;

  public direction: 'asc' | 'desc';

  public lastPagingResult?: PagingModel;

  constructor(
    public filter: T
  ) { }

  public getPagesCount() {
    return Math.ceil(this.getTotalCount() / this.pageSize);
  }

  public getStartRecord(): number {
    return (this.page - 1) * this.pageSize + 1;
  }

  public getEndRecord(): number {
    const recordIndex = this.page * this.pageSize;
    const totalCount = this.getTotalCount();
    return totalCount < recordIndex ? totalCount : recordIndex;
  }

  public getTotalCount(): number {
    if (!this.lastPagingResult) {
      return 0;
    }

    return this.lastPagingResult.totalCount;
  }

  public getPageSize(): number {
    return this.pageSize;
  }

  public getPage(): number {
    return this.page;
  }

  public setOrder(orderBy: string, orderDirection: 'asc' | 'desc' = 'asc') {
    this.orderBy = orderBy;
    this.direction = orderDirection;
  }

  public setPageSize(size: number) {
    this.pageSize = size;
    this.page = 1;
  }

  public applyChanges(changes: PagingControlChangeInterface) {
    this.page = changes.page;
    if (changes.pageSize !== this.pageSize) {
      this.setPageSize(changes.pageSize);
    }
  }

  public setPagingResult(model: PagingModel) {
    this.lastPagingResult = model;
    this.pageSize = model.pageSize;
    this.page = model.page;
    this.offset = model.offset;
  }

  public updateFilter(filter: T) {
    this.page = 1;
    this.filter = filter;
  }

  public toJSON() {
    return {
      pageSize: this.pageSize,
      page: this.page,
      filter: this.filter,
      orderBy: this.orderBy || '',
      direction: this.direction || 'asc'
    };
  }

}

export class PagingModel {
  
  page: number;
  
  pageSize: number;
  
  totalCount: number;
  
  offset: number;
  
}
`;

  public bsSize = BsSizeArray;

  public bsContext = BsContextArray;

  public pagingSize = 50;

  public currentPage = 1;

  public pagingModel = new PagingQueryModel<string>('Filter Sample');

  ngOnInit(): void {
    const pageModel = new PagingModel();
    pageModel.totalCount = 1000;
    pageModel.page = 3;
    pageModel.pageSize = 20;
    this.pagingModel.setPagingResult(pageModel);
  }

  public setPage(pageChanges: PagingControlChangeInterface) {
    this.pagingModel.applyChanges(pageChanges);
  }
}
