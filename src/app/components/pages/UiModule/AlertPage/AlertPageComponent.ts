import { Component, TemplateRef } from '@angular/core';
import { BsSizeArray } from '../../../../../argon/ui/types/BsSizeEnumType';
import { BsContextArray, BsContextEnum } from '../../../../../argon/ui/types/BsContextEnumType';
import { AlertService } from '../../../../../argon/core/services/AlertService';
import { AlertPropsInterface } from '../../../../../argon/core/interfaces/AlertPropsInterface';

@Component({
  selector: 'pg-alert-page-component',
  templateUrl: './AlertPageComponent.html'
})
export class AlertPageComponent {

  public templateSource = require('./AlertPageComponent.html');

  public componentImplementation = require('!!raw-loader!./AlertService.info');

  public codeSource = `
export class AlertComponent {

  @Input() closeable = true;

  @Output() close = new EventEmitter<void>();

}


export class AlertHeadingComponent {

}


@Injectable()
export class AlertService {

   public showAlert(props: AlertPropsInterface): void;
   
}


export interface AlertPropsInterface {
  
  message?: string;
  
  bsContext?: string;
  
  closeable?: boolean;
  
  displayTime?: number;
  
  template?: TemplateRef<any>;
  
  templateContext?: any;
  
}

export class DisplayAlertMessage implements MessageInterface {

  static DEFAULT_DISPLAY_TIME = 5000;

  static type = 'DisplayAlertMessage';

  public type = DisplayAlertMessage.type;

  public viewRef: ViewRef;

  public template?: TemplateRef<any>;

  public templateContext: any;

  constructor(
    public message: string,
    public bsContext: string = 'info',
    public closeable: boolean = true,
    public displayTime: number = DisplayAlertMessage.DEFAULT_DISPLAY_TIME
  ) { }

}
`;

  public bsSize = BsSizeArray;

  public bsContext = BsContextArray;

  constructor(
    public alertService: AlertService
  ) { }

  public showAlert(message: string) {
    this.alertService.showAlert({ message, bsContext: BsContextEnum.WARNING } as AlertPropsInterface);
  }

  public showAlertFromTemplate(template: TemplateRef<any>, templateContext: any) {
    this.alertService.showAlert({
      bsContext: BsContextEnum.SUCCESS,
      template,
      templateContext
    } as AlertPropsInterface);
  }
}
