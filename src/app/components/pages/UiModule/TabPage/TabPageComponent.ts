import { Component, TemplateRef, ViewChild } from '@angular/core';
import { BsSizeArray } from '../../../../../argon/ui/types/BsSizeEnumType';
import { BsContextArray } from '../../../../../argon/ui/types/BsContextEnumType';
import { TabModel } from '../../../../../argon/ui/models/TabModel';
import { TabLayoutComponent } from '../../../../../argon/ui/components/TabLayout/TabLayoutComponent';

@Component({
  selector: 'pg-tab-page-component',
  templateUrl: './TabPageComponent.html'
})
export class TabPageComponent {

  @ViewChild('dynamicTabLayout') dynamicTabLayout: TabLayoutComponent;

  public templateSource = require('./TabPageComponent.html');

  public codeSource = `
export class TabPageComponent {

  @ViewChild('dynamicTabLayout') dynamicTabLayout: TabLayoutComponent;

  public createNewTab(template: TemplateRef<any>, edited?: boolean) {
    const tabModel = new TabModel('Dynamic Tab Title', true);
    tabModel.changed = edited;
    const context = {
      contextVar1: 'value',
      contextVar2: false
    };

    this.dynamicTabLayout.createTab(tabModel, template, context);
  }
}

export class TabModel {

  public changed: boolean;

  public active: boolean;

  public viewRef?: EmbeddedViewRef<any>;

  constructor(
    public title: string = 'Tab title',
    public closeable: boolean = false
  ) { }

}


export class TabComponent implements OnInit {

  @Input() title: string;

  @Input() selected: boolean;

  @Input() model: TabModel;

}  
`;

  public bsSize = BsSizeArray;

  public bsContext = BsContextArray;

  public createNewTab(template: TemplateRef<any>, edited?: boolean) {
    const tabModel = new TabModel('Dynamic Tab Title', true);
    tabModel.changed = edited;
    const context = {
      contextVar1: 'value',
      contextVar2: false
    };

    this.dynamicTabLayout.createTab(tabModel, template, context);
  }


}
