import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { BsSizeArray } from '../../../../../argon/ui/types/BsSizeEnumType';
import { BsContextArray } from '../../../../../argon/ui/types/BsContextEnumType';

@Component({
  selector: 'pg-icons-page',
  templateUrl: 'IconsPageComponent.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconsPageComponent implements OnInit {

  public filter = new FormControl();

  public filterValue: Observable<string>;

  public templateSource = require('!!raw-loader!./IconsPageComponent.html');

  public codeSource = `
export class IconComponent {

  @Input() name: string;
  
  @Input() size: number;
  
}
  `;

  public bsSize = BsSizeArray;

  public bsContext = BsContextArray;

  public sampleIcon = 'account-box';

  ngOnInit(): void {
    this.filterValue = this.filter.valueChanges.pipe(debounceTime(500));
  }
}
