import { Component } from '@angular/core';
import { BsSizeArray } from '../../../../../argon/ui/types/BsSizeEnumType';
import { BsContextArray } from '../../../../../argon/ui/types/BsContextEnumType';

@Component({
  selector: 'pg-links-page-component',
  templateUrl: './LinksPageComponent.html'
})
export class LinksPageComponent {

  public templateSource = require('./LinksPageComponent.html');

  public codeSource = `

export class InnerLinkComponent {

  @Input() url: any;

  @Input() disabled: boolean;

  @Input() isAlert: boolean;

  @Input() isCard: boolean;

}

export class OuterLinkComponent extends InnerLinkComponent {

  @Input() url: any;

  @Input() disabled: boolean;

  @Input() isAlert: boolean;

  @Input() isCard: boolean;
  
}

export class MailLinkComponent {

  @Input() email: string;

}  
`;

  public bsSize = BsSizeArray;

  public bsContext = BsContextArray;

}
