import { Component } from '@angular/core';
import { BsSizeArray } from '../../../../../argon/ui/types/BsSizeEnumType';
import { BsContextArray } from '../../../../../argon/ui/types/BsContextEnumType';
import { ImageInterface } from '../../../../../argon/ui/interfaces/ImageInterface';
import { CardContentInterface } from '../../../../../argon/ui/interfaces/CardContentInterface';
import * as image from '../../../../../assets/images/card.jpg';

@Component({
  selector: 'pg-card-page-component',
  templateUrl: './CardPageComponent.html'
})
export class CardPageComponent {

  public templateSource = require('./CardPageComponent.html');

  public codeSource = `
export class CardComponent {

  @Input() image: ImageInterface;

  @Input() imageBottom: boolean;

  @Input() cardContent: CardContentInterface;

  @Input() imageOverlay: boolean;

  @Input() showHeader = false;

  @Input() showFooter = false;

  @Input() height: number;

  @Input() collapsible: boolean;

  @Input() defaultCollapsed: boolean;

  @Output() change = new EventEmitter<boolean>();
  
}

export interface CardContentInterface {

  title?: string;

  subtitle?: string;

  text?: string;

}

export class CardGroupComponent {
  
  @Input() deck: boolean;

  @Input() columns: boolean;

  @Input() group: boolean = true;
  
}
  `;

  public bsSize = BsSizeArray;

  public bsContext = BsContextArray;

  public image = { src: String(image), alt: 'Image' } as ImageInterface;

  public cardStatus: boolean;

  public cardContent = {
    title: 'Card Title',
    subtitle: 'Card Subtitle',
    text: 'Card Text'
  } as CardContentInterface;

}
