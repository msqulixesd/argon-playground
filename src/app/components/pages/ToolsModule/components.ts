import { ToolsPageComponent } from './ToolsPage/ToolsPageComponent';
import { WizardPageComponent } from './WizardPage/WizardPageComponent';

export const TOOLS_MODULE = [
  ToolsPageComponent,
  WizardPageComponent,
];
