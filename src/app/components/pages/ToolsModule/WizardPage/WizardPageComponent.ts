import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'pg-wizard-page-component',
  templateUrl: './WizardPageComponent.html'
})
export class WizardPageComponent {

  public templateSource = require('./WizardPageComponent.html');

  public codeSource = `
export class WizardComponent {

  @Input() linear: boolean;

  @Input() selectedIndex: number;

  @Input() displayHeader = true;

  @Output() selectionChange: EventEmitter<StepperSelectionEvent>;

}  

export class WizardStepComponent {

  @Input() label: string;

  @Input() stepControl: AbstractControl;
  
}
  
`;

  public control = new FormGroup({
    c1: new FormControl('', [ Validators.required ]),
    c2: new FormControl('', [ Validators.required ]),
    c3: new FormControl('', [ Validators.required ])
  });

}
