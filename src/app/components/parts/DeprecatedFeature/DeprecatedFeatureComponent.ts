import { Component, Input } from '@angular/core';

@Component({
  selector: 'pg-deprecated-feature',
  template: `
    <ar-alert arBsContext="warning" [closeable]="false">
      <ar-alert-heading>This is deprecated feature</ar-alert-heading>
      <ar-block>
        <ng-content></ng-content>
      </ar-block>
      <ar-inner-link *ngIf="newFeatureUrl" [url]="newFeatureUrl">Navigate to the new implementation</ar-inner-link>
    </ar-alert>
  `
})
export class DeprecatedFeatureComponent {

  @Input() newFeatureUrl: string;

}
