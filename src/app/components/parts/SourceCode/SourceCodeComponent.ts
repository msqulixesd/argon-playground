import { AfterViewInit, Component, ElementRef, Input, ViewChild } from '@angular/core';
import * as ts from 'highlight.js/lib/languages/typescript.js';
import * as xml from 'highlight.js/lib/languages/xml.js';

declare global {
  interface Window { hljs: any; }
}

window.hljs.registerLanguage('typescript', ts);
window.hljs.registerLanguage('xml', xml);

@Component({
  selector: 'pg-source-code',
  template: `    
    <pre #sourceCode [class]="lang">{{ source | arTrim }}<ng-content></ng-content></pre>
  `,
  preserveWhitespaces: true
})
export class SourceCodeComponent implements AfterViewInit {

  @Input() source: string;

  @Input() lang = 'xml';

  @ViewChild('sourceCode') sourceCode: ElementRef;

  ngAfterViewInit(): void {
    window.hljs.highlightBlock(this.sourceCode.nativeElement);
  }
}
