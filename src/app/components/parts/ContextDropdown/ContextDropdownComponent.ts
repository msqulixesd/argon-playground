import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { toArray } from 'lodash';
import { BsContextEnum } from '../../../../argon/ui/types/BsContextEnumType';
import { Subscription } from 'rxjs';

@Component({
  selector: 'pg-context-dropdown',
  template: `
    <ar-select [control]="control" [list]="list"></ar-select>
  `
})
export class ContextDropdownComponent implements OnInit, OnDestroy {

  @Input() defaultValue = 'primary';

  @Output() changeContext = new EventEmitter<string>();

  public control = new FormControl();

  public list = toArray(BsContextEnum);

  private subscription: Subscription;

  ngOnInit(): void {
    //
    this.control.setValue(this.defaultValue);
    this.subscription = this.control.valueChanges.subscribe((value: any) => {
      this.changeContext.emit(value);
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
