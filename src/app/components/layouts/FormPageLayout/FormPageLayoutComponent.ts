import { PageLayoutComponent } from '../PageLayout/PageLayoutComponent';
import { Component } from '@angular/core';

@Component({
  selector: 'pg-form-page-layout',
  template: `
    <ar-page-header *ngIf="this.activatedRoute.data | async as data">
      {{ data?.title }}
    </ar-page-header>
    <ar-block>
      <ar-inner-link url="/forms/overview">View Form Example</ar-inner-link>
    </ar-block>
    <router-outlet></router-outlet>
  `
})
export class FormPageLayoutComponent extends PageLayoutComponent {

}
