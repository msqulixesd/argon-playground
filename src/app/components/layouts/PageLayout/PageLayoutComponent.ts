import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'pg-page-layout',
  template: `
    <ar-page-header *ngIf="this.activatedRoute.data | async as data">
      {{ data?.title }}
    </ar-page-header>
    <router-outlet></router-outlet>
  `
})
export class PageLayoutComponent {

  public title: string;

  constructor(
    public activatedRoute: ActivatedRoute
  ) { }
}
