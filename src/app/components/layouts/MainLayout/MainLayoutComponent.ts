import { AfterViewInit, Component, OnInit } from '@angular/core';
import { omit } from 'lodash';
import { MenuService } from '../../../services/MenuService';
import { MenuItemInterface } from '../../../interfaces/MenuItemInterface';
import { IconService } from '../../../../argon/ui/services/IconService';
import * as icons from 'material-design-icons-svg/paths.json';

// noinspection TsLint
@Component({
  selector: 'pg-main-layout',
  templateUrl: './MainLayoutComponent.html',
  styles: [`
    .footer {
      padding: 10px 0;   
    }
  `]
})
export class MainLayoutComponent implements OnInit {

  public menu: Array<MenuItemInterface> = [];

  public context: string;

  public get defaultContext(): string {
    return this.context || 'dark';
  }

  public get iconContext(): string {
    return this.defaultContext === 'dark' ? 'light' : 'dark';
  }

  constructor(
    menuService: MenuService,
    private iconService: IconService
  ) {
    this.menu = menuService.getMenu();
  }

  ngOnInit(): void {
    this.iconService.addMany(omit(icons, ['default']));
  }
}
