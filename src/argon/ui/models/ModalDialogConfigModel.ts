import { without, concat, indexOf, join } from 'lodash';
import { ModalDialogConfigInterface } from '../interfaces/ModalDialogConfigInterface';

export class ModalDialogConfigModel implements ModalDialogConfigInterface {

  public set large(value: boolean) {
    this.toggleClass('modal-lg', value);
  }

  public set small(value: boolean) {
    this.toggleClass('modal-sm', value);
  }

  public set customClass(value: string) {
    this.toggleClass(value, true);
  }

  public get className(): string {
    return join(this._classes, ' ');
  }

  public backdropClickClose = true;

  public centered = false;

  private _classes: Array<string> = [];

  private toggleClass(className: string, state: boolean) {
    if (indexOf(this._classes, className) >= 0 && state) {
      return;
    }

    this._classes = state
      ? concat(this._classes, className)
      : without(this._classes, className);
  }
}
