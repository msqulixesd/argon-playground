import { Component } from '@angular/core';

@Component({
  selector: 'ar-simple-layout',
  template: `
    <header>
      <ng-content select="[header]"></ng-content>
    </header>
    <section>
      <ng-content></ng-content>
    </section>
    <footer>
      <ng-content select="[footer]"></ng-content>
    </footer>
  `,
  styleUrls: ['SimpleLayoutComponent.scss']
})
export class SimpleLayoutComponent {

}
