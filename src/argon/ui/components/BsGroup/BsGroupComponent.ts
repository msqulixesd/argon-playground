import { Component, Input } from '@angular/core';

@Component({
  selector: 'ar-bs-group, [arBsGroup]',
  template: `    
    <ng-content></ng-content>`,
})
export class BsGroupComponent {

  @Input() bsSize: string;

  @Input() bsContext: string;

  @Input() bsBgContext: string;

  @Input() bsTextContext: string;

  @Input() textAlign: string;

}
