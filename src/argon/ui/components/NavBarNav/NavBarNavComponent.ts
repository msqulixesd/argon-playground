import { Component } from '@angular/core';

@Component({
  selector: 'ar-nav-bar-nav',
  template: `
    <ul class="navbar-nav">
      <ng-content></ng-content>
    </ul>
  `
})
export class NavBarNavComponent {

}
