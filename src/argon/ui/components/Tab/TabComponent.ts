import { Component, Input, OnInit, ViewChild, ViewContainerRef, ViewRef } from '@angular/core';
import { TabLayoutComponent } from '../TabLayout/TabLayoutComponent';
import { TabModel } from '../../models/TabModel';
import { animate, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'ar-tab',
  template: `
    <section *ngIf="isActive" [@toggleServiceState]>
      <ng-content></ng-content>
    </section>
  `,
  animations: [trigger('toggleServiceState', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('200ms ease-in', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ opacity: 1, position: 'absolute', top: '5px', right: '5px', left: '5px' }),
        animate('200ms ease-out', style({ opacity: 0 }))
      ])
    ])
  ],
})
export class TabComponent implements OnInit {

  @ViewChild('dynamicContent', { read: ViewContainerRef }) dynamicContent: ViewContainerRef;

  @Input() title: string;

  @Input() selected: boolean;

  @Input() model: TabModel;

  get isActive() {
    return (this.model && this.model.active);
  }

  public viewRef: ViewRef;

  constructor(
    private tabLayout: TabLayoutComponent
  ) {}

  ngOnInit() {
    if (this.model) {
      return;
    }

    this.model = new TabModel(this.title);
    this.tabLayout.registerTab(this.model);
    if (this.selected) {
      this.tabLayout.selectTab(this.model);
    }
  }

  public updateTitle(title: string) {
    if (this.model) {
      this.model.title = title;
    }
  }
}
