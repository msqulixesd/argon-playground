import { map, range, concat } from 'lodash';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PageLinkInterface } from '../../interfaces/PageLinkInterface';
import { ContextualService } from '../../services/ContextualService';

@Component({
  selector: 'ar-pagination',
  template: `
      <nav *ngIf="isVisible" class="noselect">
          <ul class="pagination" [arBsSize]="contextualService.bsSize">
              <li *ngFor="let item of pageLinks"
                  [class.active]="isActivePage(item)"
                  [class.disabled]="item.disabled"
                  class="page-item"
              >
                  <a class="pointer page-link" (click)="handlePageClick(item)">
                      {{item.title}}
                  </a>
              </li>
          </ul>
      </nav>
  `,
  styles: [
    ':host { display: inline-block; vertical-align: middle; }',
    '.pagination { margin: 0; }'
  ],
  providers: [ContextualService]
})
export class PaginationComponent implements OnInit {

  static PREV_TITLE = '«';
  static NEXT_TITLE = '»';
  static EMPTY_TITLE = '...';

  static MAX_DISPLAY = 15;
  static EDGE_COUNT = 6;
  static MIN_COUNT = 3;

  @Input() set total(val: number) {
    this._total = Number(val);
    this.pageLinks = this.createPages(this.total);
  }
  get total(): number { return this._total; }

  @Input() set current(val: number) {
    this._currentPage = Number(val);
    this.pageLinks = this.createPages(this.total);
  }
  get current(): number { return this._currentPage; }

  @Output() onPageSelect = new EventEmitter<number>();

  get isVisible(): boolean {
    return this.total > 0 && this.current <= this.total && this.current > 0;
  }

  public pageLinks: Array<PageLinkInterface>;

  private _currentPage: number;
  private _total = 0;

  constructor(
    public contextualService: ContextualService
  ) { }

  ngOnInit() {
    this.pageLinks = this.createPages(this.total);
  }

  public handlePageClick(page: PageLinkInterface) {
    if (!page || page.disabled || this.isActivePage(page)) {
      return;
    }

    this.onPageSelect.emit(page.page);
  }

  public isActivePage(page: PageLinkInterface): boolean {
    return !page.disabled && !page.common && this.current === page.page;
  }

  private createPages(total: number): Array<PageLinkInterface> {
    if (total <= PaginationComponent.MAX_DISPLAY) {
      return map(range(1, total + 1), (page: number) => this.createPageItem(page));
    }

    if (this.current < PaginationComponent.EDGE_COUNT - 1) {
      return concat(
        [this.createPageItem(this.current - 1, PaginationComponent.PREV_TITLE, this.current < 2, true)],
        map(range(1, PaginationComponent.EDGE_COUNT), (page: number) => this.createPageItem(page)),
        [this.createPageItem(0, PaginationComponent.EMPTY_TITLE, true, true)],
        map(range(total - PaginationComponent.MIN_COUNT + 1, total + 1), (page: number) => this.createPageItem(page)),
        [this.createPageItem(this.current + 1, PaginationComponent.NEXT_TITLE, false, true)],
      );
    }

    if (this.total - PaginationComponent.EDGE_COUNT + 2 < this.current) {
      return concat(
        [this.createPageItem(this.current - 1, PaginationComponent.PREV_TITLE, false, true)],
        map(range(1, PaginationComponent.MIN_COUNT + 1), (page: number) => this.createPageItem(page)),
        [this.createPageItem(0, PaginationComponent.EMPTY_TITLE, true, true)],
        map(range(total - PaginationComponent.EDGE_COUNT + 2, total + 1), (page: number) => this.createPageItem(page)),
        [this.createPageItem(this.current + 1, PaginationComponent.NEXT_TITLE, false, true)],
      );
    }

    return concat(
      [this.createPageItem(this.current - 1, PaginationComponent.PREV_TITLE, false, true)],
      map(range(1, PaginationComponent.MIN_COUNT), (page: number) => this.createPageItem(page)),
      [this.createPageItem(0, PaginationComponent.EMPTY_TITLE, true, true)],
      map(range(this.current - 1, this.current + 2), (page: number) => this.createPageItem(page)),
      [this.createPageItem(0, PaginationComponent.EMPTY_TITLE, true, true)],
      map(range(total - PaginationComponent.MIN_COUNT + 2, total + 1), (page: number) => this.createPageItem(page)),
      [this.createPageItem(this.current + 1, PaginationComponent.NEXT_TITLE, false, true)],
    );
  }

  private createPageItem = (
    page: number, title: string = null, disabled: boolean = false, common: boolean = false
  ): PageLinkInterface => {
    return {
      title: title || String(page),
      page,
      disabled: disabled || (common && page === this.current) || page < 0 || page > this.total,
      common
    };
  }
}
