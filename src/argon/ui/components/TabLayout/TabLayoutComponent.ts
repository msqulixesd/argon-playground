import {
  Component, TemplateRef, ViewChild,
  ViewContainerRef
} from '@angular/core';
import { TabLayoutViewModel } from './TabLayoutViewModel';
import { TabModel } from '../../models/TabModel';
import { Subject } from 'rxjs';

@Component({
  selector: 'ar-tab-layout',
  templateUrl: 'TabLayout.component.html',
  styleUrls: ['TabLayout.component.scss']
})
export class TabLayoutComponent {

  @ViewChild('content', { read: ViewContainerRef }) content: ViewContainerRef;

  public tabRemove = new Subject<TabModel>();
  public tabSelect = new Subject<TabModel>();
  public tabCreate = new Subject<TabModel>();

  public viewModel: TabLayoutViewModel;

  constructor() {
    this.viewModel = new TabLayoutViewModel();
  }

  public registerTab(tab: TabModel) {
    this.viewModel.addTab(tab);
  }

  public removeTab(tab: TabModel) {
    if (tab.changed) {
      this.viewModel.tabToClose = tab;
    } else  {
      this.deleteTab(tab);
    }
  }

  public exist(tab: TabModel): boolean {
    return this.viewModel.exist(tab);
  }

  public deleteTab(tab: TabModel) {
    this.viewModel.removeTab(tab);
    if (tab.viewRef) {
      const index = this.content.indexOf(tab.viewRef);
      this.content.remove(index);
    }

    this.tabRemove.next(tab);
    this.viewModel.tabToClose = null;
  }

  public createTab(tab: TabModel, template: TemplateRef<any>, context?: any) {
    if (this.exist(tab)) {
      this.selectTab(tab);
      return;
    }

    tab.viewRef = this.content.createEmbeddedView(template, { $implicit: context, tab });
    this.registerTab(tab);
    this.selectTab(tab);
    this.tabCreate.next(tab);
  }

  public selectTab(tab: TabModel) {
    this.viewModel.selectTab(tab);
    this.tabSelect.next(tab);
  }

}
