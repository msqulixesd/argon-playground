import {
  ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output,
  SimpleChanges
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { BsSizeEnum } from '../../types/BsSizeEnumType';
import { ContextualService } from '../../services/ContextualService';

@Component({
  selector: 'ar-paging-list-size',
  template: `
    <div *ngIf='form' [formGroup]="form">
        <select class="form-control" formControlName="size" [arBsSize]="contextualService.bsSize">
            <option *ngFor="let item of list" [value]="item">{{item}}</option>
        </select>
    </div>
  `,
  styleUrls: ['PagingListSize.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ContextualService]
})
export class PagingListSizeComponent implements OnInit, OnChanges, OnDestroy {

  @Input() size: number;

  @Input() list: Array<number> = [10, 20, 50, 100];

  @Input() bsSize = BsSizeEnum.SM;

  @Output() onSizeChange = new EventEmitter<number>();

  public form: FormGroup;

  private changeSubscription: Subscription;

  constructor(
    public contextualService: ContextualService
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    const { size } = changes;
    if (!size.firstChange && size.previousValue !== size.currentValue) {
      this.form.reset({ size: size.currentValue }, { emitEvent: false });
    }
  }

  ngOnInit(): void {
    const size = new FormControl(this.size);
    this.changeSubscription = size.valueChanges
      .subscribe((val: number) => { this.onSizeChange.emit(val); });
    this.form = new FormGroup({ size });
  }

  ngOnDestroy(): void {
    this.changeSubscription.unsubscribe();
  }
}
