import { Component, HostBinding} from '@angular/core';
import { ToggleService } from '../../services/ToggleService';
import { ContextualService } from '../../services/ContextualService';

@Component({
  selector: 'ar-nav-bar',
  template: `
    <div class="navbar-brand">
      <ng-content select="[navbar-brand]"></ng-content>
    </div>
    <button class="navbar-toggler" (click)="toggleService.toggle()">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="navbar-collapse" [@toggleServiceState]="toggleService.state">
      <ng-content></ng-content>
      <div class="form-inline">
        <ng-content select="[form-inline]"></ng-content>
      </div>
    </div>
  `,
  animations: [ ToggleService.ANIMATION_HEIGHT ],
  providers: [ ToggleService, ContextualService ],
  styles: [`
    .form-inline {
      margin-left: auto;
    }
  `]
})
export class NavBarComponent {

  static BASE_CLASS = 'navbar';

  @HostBinding('class.navbar') baseClass = true;

  get navBarClass(): string {
    return '';
    /*switch (this.arBsContext) {
      case BsContextEnum.WARNING:
      case BsContextEnum.SECONDARY:
      case BsContextEnum.PRIMARY:
      case BsContextEnum.DARK:
      case BsContextEnum.DANGER:
      case BsContextEnum.SUCCESS:
        return `${this.baseClass}-${BsContextEnum.DARK}`;
      case BsContextEnum.LINK:
      case BsContextEnum.LIGHT:
      case BsContextEnum.DEFAULT:
      case BsContextEnum.INFO:
      default:
        return `${this.baseClass}-${BsContextEnum.LIGHT}`;
    }*/
  }

  constructor(
    public toggleService: ToggleService,
    public contextualService: ContextualService
  ) {
    this.contextualService.bsSizePrefix = 'expand';
    this.contextualService.baseClass = NavBarComponent.BASE_CLASS;
    toggleService.open();
  }

}
