import { Component, Input } from '@angular/core';

@Component({
  selector: 'ar-figure',
  template: `
    <figure class="figure">
      <ng-content></ng-content>
      <figcaption class="figure-caption" [arTextAlign]="captionAlign">
        <ng-content select="[caption]"></ng-content>
      </figcaption>
    </figure>
  `
})
export class FigureComponent {

  @Input() captionAlign: string;

}
