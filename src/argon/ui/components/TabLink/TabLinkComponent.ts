import { Component, EventEmitter, Input, Output } from '@angular/core';
import { TabModel } from '../../models/TabModel';

@Component({
  selector: 'ar-tab-link',
  template: `
      <ng-content></ng-content>
      <button type="button" class="close" (click)="closeTabHandler($event)" *ngIf="model.closeable">
          <span aria-hidden="true">×</span>
      </button>
  `,
  styles: [
    `:host { white-space: nowrap; }`,
    `.close { position: absolute; right: 3px; top: 0; }`
  ]
})
export class TabLinkComponent {

  @Input() model: TabModel;

  @Output() onClose = new EventEmitter<TabModel>();

  closeTabHandler(event: any) {
    event.stopPropagation();
    this.onClose.emit(this.model);
  }

}
