import { Component } from '@angular/core';

@Component({
  selector: 'ar-dropdown-divider',
  template: `
    <small arBsTextContext="muted">
      <ng-content></ng-content>
    </small>
    <div class="dropdown-divider"></div>
  `
})
export class DropdownDividerComponent {

}
