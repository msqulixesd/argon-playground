import { Component, Input, TemplateRef, ViewChild } from '@angular/core';
import { ModalService } from '../../services/ModalService';
import { modalServiceFactory } from '../../factories/modalServiceFactory';
import { ModalDialogConfigModel } from '../../models/ModalDialogConfigModel';
import { ToggleService } from '../../services/ToggleService';
import { LogService } from '../../../core/services/LogService';

@Component({
  selector: 'ar-modal',
  template: `
    <ng-template #modalTemplate let-context>
        <div class="modal-content" [@toggleServiceState]>
            <div class="modal-header" *ngIf="showHeader">
                <ng-container *ngIf="headerTemplate">
                    <ng-container *ngTemplateOutlet="headerTemplate; context: { $implicit: context }">
                    </ng-container>
                </ng-container>
                <ng-content select="[header]"></ng-content>
                <button type="button" class="close" (click)="hide()" >
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ng-container *ngIf="bodyTemplate">
                    <ng-container *ngTemplateOutlet="bodyTemplate; context: { $implicit: context }"></ng-container>
                </ng-container>
                <ng-content></ng-content>
            </div>
            <div class="modal-footer" *ngIf="showFooter">
                <ng-container *ngIf="footerTemplate">
                    <ng-container *ngTemplateOutlet="footerTemplate; context: { $implicit: context }"></ng-container>
                </ng-container>
                <ng-content select="[footer]"></ng-content>
            </div>
        </div>
    </ng-template>
  `,
  styles: [`
    .modal-content {
        word-wrap: break-word;
        overflow-wrap: break-word;
    }
  `],
  providers: [{
    provide: ModalService,
    useFactory: modalServiceFactory,
    deps: [ LogService ]
  }],
  animations: [
    ToggleService.ANIMATION_OPACITY
  ]
})
export class ModalComponent {

  static createConfig() { return new ModalDialogConfigModel(); }

  @ViewChild('modalTemplate', { read: TemplateRef }) modalTemplate: TemplateRef<any>;

  @Input() headerTemplate: TemplateRef<any>;

  @Input() bodyTemplate: TemplateRef<any>;

  @Input() footerTemplate: TemplateRef<any>;

  @Input() config: ModalDialogConfigModel = ModalComponent.createConfig();

  @Input() showHeader = true;

  @Input() showFooter = true;

  constructor(
    private modalService: ModalService
  ) { }

  show(context?: any) {
    this.modalService.showModal(this.modalTemplate, this.config, context);
  }

  hide() {
    this.modalService.hideModal();
  }

}
