import { Component } from '@angular/core';

@Component({
  selector: 'ar-alert-heading',
  template: `
    <span class="h4 alert-heading">
      <ng-content></ng-content>
    </span>
  `
})
export class AlertHeadingComponent {

}
