import { Component, HostBinding, Input } from '@angular/core';
import { ContextualService } from '../../services/ContextualService';

@Component({
  selector: 'ar-progress-bar',
  template: `
    <div class="progress-bar"
         [class.progress-bar-striped]="striped"
         [class.progress-bar-animated]="active"
         role="progressbar"
         [attr.aria-valuenow]="value"
         aria-valuemin="0"
         aria-valuemax="100"
         [ngStyle]="style"
         [arBsBgContext]="contextualService.bsContext"
    >
      <ng-container *ngIf="showValue">
        {{ valueString }}
      </ng-container>
    </div>
  `,
  providers: [ContextualService]
})
export class ProgressBarComponent {

  static BASE_CLASS = 'progress';

  @Input() showValue = false;

  @Input() striped = false;

  @Input() active = false;

  @Input() set height(val: number) {
    this.hostStyle = val;
  }

  @Input() set value(val: number) {
    this.ariaValueNow = String(val);
    this._value = val;
  }
  get value(): number { return this._value; }

  @HostBinding('class.progress') baseClass = true;

  @HostBinding('attr.role') role = 'progressbar';

  @HostBinding('attr.aria-valuenow') ariaValueNow = '0';
  @HostBinding('attr.aria-valuemin') ariaValueMin = '0';
  @HostBinding('attr.aria-valuemax') ariaValueMax = '100';

  @HostBinding('style.height.px') hostStyle: number;

  public get valueString(): string {
    if (this.value < 0 || this.value > 100) {
      throw new Error('ProgressBarComponent: value should be a number between 0 and 100!');
    }

    return `${this.value}%`;
  }

  public get style(): any {
    return { width: this.valueString };
  }

  private _value = 0;

  constructor(
    public contextualService: ContextualService
  ) {
    contextualService.baseClass = ProgressBarComponent.BASE_CLASS;
  }
}
