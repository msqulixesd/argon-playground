import { Component } from '@angular/core';

@Component({
  selector: 'ar-page-header',
  template: `
      <ar-block class="page-header clearfix border-bottom">
          <div class="pull-left">
            <h1>
                <ng-content></ng-content>
            </h1>
          </div>
          <div class="pull-right">
            <ng-content select="[side-content]"></ng-content>
          </div>
      </ar-block>
  `
})
export class PageHeaderComponent { }
