import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ContextualService } from '../../services/ContextualService';

@Component({
  selector: 'ar-button-group',
  template: `    
    <div class="btn-toolbar"
         [class.btn-group-vertical]="vertical"
         [class.btn-group]="!vertical"
    >
      <ng-content></ng-content>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ ContextualService ],
})
export class ButtonGroupComponent {

  @Input() vertical: boolean;

  get bsContext(): string { return this.contextualService.bsContext; }

  get bsContextPrefix(): string { return this.contextualService.bsContextPrefix; }

  get bsSize(): string { return this.contextualService.bsSize; }

  constructor(
    protected contextualService: ContextualService
  ) { }

}
