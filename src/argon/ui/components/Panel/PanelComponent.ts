import { Component, EventEmitter, Input, Output } from '@angular/core';
import { BsContextType } from '../../types/BsContextEnumType';

@Component({
  selector: 'ar-panel',
  template: `
    <ar-card
      [arBsBgContext]="bsContext"
      [collapsible]="collapsible"
      [showHeader]="showHeader"
      [showFooter]="showFooter"
      [defaultCollapsed]="defaultCollapsed"
      [height]="height"
      (change)="change.emit($event)"
    >
      <ng-container header>
        <ng-content select="[header]" #header></ng-content>
      </ng-container>
      <ng-content></ng-content>
      <ng-container footer>
        <ng-content select="[footer]" #header></ng-content>
      </ng-container>
    </ar-card>
  `
})
/**
 * @deprecated
 * @see ar-card
 */
export class PanelComponent {

  @Input() bsContext: BsContextType;

  @Input() collapsible: boolean;

  @Input() showHeader = false;

  @Input() showFooter = false;

  @Input() defaultCollapsed: boolean;

  @Input() height: number;

  @Output() change = new EventEmitter<boolean>();

}
