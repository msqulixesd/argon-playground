import { ChangeDetectorRef, Component, ElementRef, Input} from '@angular/core';
import { ToggleService } from '../../services/ToggleService';
import { DropdownComponent } from '../Dropdown/DropdownComponent';
import { LayoutComponent } from '../Layout/LayoutComponent';
import { ContextualService } from '../../services/ContextualService';

@Component({
  selector: 'ar-dropdown-menu',
  template: `
      <div class="dropdown" [class.dropup]="dropUp" [class.full-width]="fullWidth">
        <ar-button class="dropdown-toggle"
                  [arBsContext]="contextualService.bsContext"
                  [arBsSize]="contextualService.bsSize"
                  (click)="toggle()"
        >
          <ng-content select="[title]"></ng-content>
        </ar-button>
        <ul
          class="dropdown-menu"
          [@toggleServiceState]="toggleService.state"
          *ngIf="opened"
        >
          <ng-content></ng-content>
        </ul>
      </div>
  `,
  styleUrls: ['../Dropdown/Dropdown.component.scss'],
  providers: [ ContextualService, ToggleService ],
  animations: [ ToggleService.ANIMATION_OPACITY ]
})
export class DropdownMenuComponent extends DropdownComponent {

  @Input() disabled: boolean;

  @Input() fullWidth: boolean;

  @Input() dropUp: boolean;

  constructor(
    public contextualService: ContextualService,
    protected element: ElementRef,
    protected layout: LayoutComponent,
    protected ref: ChangeDetectorRef,
    public toggleService: ToggleService
  ) {
    super(element, layout, ref, toggleService);
  }

}
