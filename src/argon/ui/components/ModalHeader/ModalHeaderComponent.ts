import { Component } from '@angular/core';

@Component({
  selector: 'ar-modal-header',
  template: `
    <span class="modal-header-content">
      <ng-content></ng-content>
    </span>
  `,
  styles: [`
    .modal-header-content {
      font-weight: bold;
      font-size: 110%;
    }
  `]
})
export class ModalHeaderComponent {

}
