import { Component, HostBinding, HostListener, Input, Optional} from '@angular/core';
import { DropdownComponent } from '../Dropdown/DropdownComponent';
import { DropdownMenuComponent } from '../DropdownMenu/DropdownMenuComponent';
import { ContextualService } from '../../services/ContextualService';

@Component({
  selector: 'ar-dropdown-menu-item',
  template: `
    <div class="prefix-block" [arBsBgContext]="contextualService.bsContext"></div>
    <ng-content></ng-content>
    <div class="pull-right">
      <ng-content select="[right]"></ng-content>
    </div>
  `,
  styleUrls: ['DropdownMenuItemComponent.scss'],
  providers: [ ContextualService ]
})
export class DropdownMenuItemComponent {

  @Input() set disabled(val: boolean) {
    this.disabledClassName = val;
  }

  @Input() closeOnSelect = true;

  @HostBinding('class.dropdown-item') className = true;

  @HostBinding('class.disabled') disabledClassName = false;

  public get parentDropdown(): DropdownComponent {
    if (this.dropdown) {
      return this.dropdown;
    }

    if (this.dropdownMenu) {
      return this.dropdownMenu;
    }

    return null;
  }

  constructor(
    @Optional() private dropdown: DropdownComponent,
    @Optional() private dropdownMenu: DropdownMenuComponent,
    public contextualService: ContextualService
  ) { }

  @HostListener('click', ['$event'])
  public handleClick(e: MouseEvent) {
    e.stopPropagation();

    if (this.disabledClassName) {
      return;
    }

    if (this.closeOnSelect && this.parentDropdown) {
      this.parentDropdown.close();
    }
  }

}
