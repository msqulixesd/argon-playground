import { Component, EventEmitter, Input, Output, OnInit, HostBinding } from '@angular/core';
import { ImageInterface } from '../../interfaces/ImageInterface';
import { CardContentInterface } from '../../interfaces/CardContentInterface';
import { ToggleService } from '../../services/ToggleService';
import { ContextualService } from '../../services/ContextualService';

@Component({
  selector: 'ar-card, [arCard]',
  templateUrl: 'CardComponent.html',
  styleUrls: ['CardComponent.scss'],
  animations: [ ToggleService.ANIMATION_HEIGHT ],
  providers: [ ToggleService, ContextualService ]
})
export class CardComponent implements OnInit {

  static BASE_CLASS = 'card';

  @Input() image: ImageInterface;

  @Input() imageBottom: boolean;

  @Input() cardContent: CardContentInterface;

  @Input() imageOverlay: boolean;

  @Input() showHeader = false;

  @Input() showFooter = false;

  @Input() height: number;

  @Input() collapsible: boolean;

  @Input() defaultCollapsed: boolean;

  @Output() change = new EventEmitter<boolean>();

  @HostBinding('class.card') className = true;

  public get hasTopImage(): boolean { return Boolean(this.image) && !this.imageBottom; }

  public get hasBottomImage(): boolean { return Boolean(this.image) && this.imageBottom; }

  constructor(
    public toggleService: ToggleService,
    contextualService: ContextualService
  ) {
    contextualService.baseClass = CardComponent.BASE_CLASS;
  }

  ngOnInit() {
    this.toggleService.state = this.collapsible && this.defaultCollapsed
      ? ToggleService.INACTIVE_STATE
      : ToggleService.ACTIVE_STATE;
  }

  public toggle() {
    if (!this.collapsible) {
      return;
    }

    this.toggleService.toggle();
    this.change.emit(this.toggleService.boolState);
  }
}
