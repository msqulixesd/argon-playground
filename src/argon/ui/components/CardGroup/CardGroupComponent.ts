import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'ar-card-group, [arCardGroup]',
  template: `
    <ng-content></ng-content>
  `,
  styleUrls: ['CardGroupComponent.scss']
})
export class CardGroupComponent {

  @Input() set deck(val: boolean) {
    this.isColumns = !val;
    this.isGroup = !val;
    this.isDeck = val;
  }

  @Input() set columns(val: boolean) {
    this.isColumns = val;
    this.isGroup = !val;
    this.isDeck = !val;
  }

  @Input() set group(val: boolean) {
    this.isColumns = !val;
    this.isGroup = val;
    this.isDeck = !val;
  }

  @HostBinding('class.card-group') isGroup = true;
  @HostBinding('class.card-deck') isDeck = false;
  @HostBinding('class.card-columns') isColumns = false;

}
