import { Component, Input } from '@angular/core';
import { PagingInterface } from '../../interfaces/PagingInterface';

@Component({
  selector: 'ar-paging-text',
  template: `
    <small *ngIf="shouldDisplay">
      Showing {{ paging.getStartRecord() }} to {{ paging.getEndRecord() }} of {{ paging.getTotalCount() }} entries
    </small>
  `,
  styles: [
    `:host {
        display: inline-block;
        margin: 0 1em;
    }`
  ]
})
export class PagingTextComponent {

  @Input() paging: PagingInterface;

  public get shouldDisplay(): boolean {
    return this.paging && this.paging.getTotalCount() > 0;
  }
}
