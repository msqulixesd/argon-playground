import { values } from 'lodash';

const INFO = 'info';

const PRIMARY = 'primary';

const SECONDARY = 'secondary';

const SUCCESS = 'success';

const WARNING = 'warning';

const DANGER = 'danger';

const DEFAULT = '';

const LIGHT = 'light';

const DARK = 'dark';

const LINK = 'link';

const WHITE = 'white';

const MUTED = 'muted';

export const BsTextContextEnum = { INFO, PRIMARY, SUCCESS, WARNING, DANGER, DEFAULT, SECONDARY, LIGHT, DARK, LINK, WHITE, MUTED };

export type BsTextContextType = keyof typeof BsTextContextEnum;

export const BsTextContextArray = values(BsTextContextEnum);
