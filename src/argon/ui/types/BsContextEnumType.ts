import { values } from 'lodash';

const INFO = 'info';

const PRIMARY = 'primary';

const SECONDARY = 'secondary';

const SUCCESS = 'success';

const WARNING = 'warning';

const DANGER = 'danger';

const DEFAULT = 'default';

const LIGHT = 'light';

const DARK = 'dark';

const LINK = 'link';

export const BsContextEnum = { INFO, PRIMARY, SUCCESS, WARNING, DANGER, DEFAULT, SECONDARY, LIGHT, DARK, LINK };

export type BsContextType = keyof typeof BsContextEnum;

export const BsContextArray = values(BsContextEnum);
