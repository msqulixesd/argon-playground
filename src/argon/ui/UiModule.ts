import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonComponent } from './components/Button/ButtonComponent';
import { BsContextDirective } from './directives/BsContextDirective';
import { SimpleLayoutComponent } from './layouts/SimpleLayout/SimpleLayoutComponent';
import { BsGroupComponent } from './components/BsGroup/BsGroupComponent';
import { BsSizeDirective } from './directives/BsSizeDirective';
import { ButtonGroupComponent } from './components/ButtonGroup/ButtonGroupComponent';
import { BsTextContextDirective } from './directives/BsTextContextDirective';
import { BsBgContextDirective } from './directives/BsBgContextDirective';
import { NavBarComponent } from './components/NavBar/NavBarComponent';
import { NavBarNavComponent } from './components/NavBarNav/NavBarNavComponent';
import { NavBarNavItemComponent } from './components/NavBarNavItem/NavBarNavItemComponent';
import { LayoutComponent } from './components/Layout/LayoutComponent';
import { DropdownComponent } from './components/Dropdown/DropdownComponent';
import { DropdownMenuComponent } from './components/DropdownMenu/DropdownMenuComponent';
import { NavBarNavDropdownItemComponent } from './components/NavBarNavDropdownItem/NavBarNavDropdownItemComponent';
import { DropdownMenuItemComponent } from './components/DropdownMenuItem/DropdownMenuItemComponent';
import { IconService } from './services/IconService';
import { IconComponent } from './components/Icon/IconComponent';
import { FigureComponent } from './components/Figure/FigureComponent';
import { TextAlignDirective } from './directives/TextAlignDirective';
import { InnerLinkComponent } from './components/InnerLink/InnerLinkComponent';
import { RouterModule } from '@angular/router';
import { BlockComponent } from './components/Block/BlockComponent';
import { TabComponent } from './components/Tab/TabComponent';
import { TabLinkComponent } from './components/TabLink/TabLinkComponent';
import { TabLayoutComponent } from './components/TabLayout/TabLayoutComponent';
import { DropdownDividerComponent } from './components/DropdownDivider/DropdownDividerComponent';
import { AlertComponent } from './components/Alert/AlertComponent';
import { AlertAreaComponent } from './components/AlertArea/AlertAreaComponent';
import { CoreModule } from '../core/CoreModule';
import { AlertHeadingComponent } from './components/AlertHeading/AlertHeadingComponent';
import { LoremIpsumComponent } from './components/LoremIpsum/LoremIpsumComponent';
import { HelpBlockComponent } from './components/HelpBlock/HelpBlockComponent';
import { OuterLinkComponent } from './components/OuterLink/OuterLinkComponent';
import { PageHeaderComponent } from './components/PageHeader/PageHeaderComponent';
import { PagingListSizeComponent } from './components/PagingListSize/PagingListSizeComponent';
import { PaginationComponent } from './components/Pagination/PaginationComponent';
import { PagingTextComponent } from './components/PagingText/PagingTextComponent';
import { PagingControlComponent } from './components/PagingControl/PagingControlComponent';
import { MailLinkComponent } from './components/MailLink/MailLinkComponent';
import { ModalLayoutComponent } from './components/ModalLayout/ModalLayoutComponent';
import { ModalComponent } from './components/Modal/ModalComponent';
import { ModalHeaderComponent } from './components/ModalHeader/ModalHeaderComponent';
import { PanelComponent } from './components/Panel/PanelComponent';
import { CardComponent } from './components/Card/CardComponent';
import { WidthDirective } from './directives/WidthDirective';
import { BorderDirective } from './directives/BorderDirective';
import { CardGroupComponent } from './components/CardGroup/CardGroupComponent';
import { DeprecatedDirective } from './directives/DeprecatedDirective';
import { ProgressBarComponent } from './components/ProgressBar/ProgressBarComponent';
import { SpinnerDirective } from './directives/SpinnerDirective';
import { SpinnerComponent } from './components/Spinner/SpinnerComponent';
import { SwitchComponent } from './components/Switch/SwitchComponent';
import { RowComponent } from './components/Row/RowComponent';
import { ColComponent } from './components/Col/ColComponent';
import { ScrollableDirective } from './directives/ScrollableDirective';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    CoreModule
  ],
  declarations: [
    AlertComponent,
    AlertAreaComponent,
    AlertHeadingComponent,
    HelpBlockComponent,
    LayoutComponent,
    LoremIpsumComponent,
    SimpleLayoutComponent,
    ButtonComponent,
    ButtonGroupComponent,
    BsGroupComponent,
    NavBarComponent,
    NavBarNavComponent,
    NavBarNavItemComponent,
    NavBarNavDropdownItemComponent,
    BsSizeDirective,
    BsContextDirective,
    BsBgContextDirective,
    BsTextContextDirective,
    TextAlignDirective,
    WidthDirective,
    BorderDirective,
    DeprecatedDirective,
    SpinnerDirective,
    ScrollableDirective,
    DropdownComponent,
    DropdownMenuComponent,
    DropdownMenuItemComponent,
    DropdownDividerComponent,
    IconComponent,
    FigureComponent,
    InnerLinkComponent,
    OuterLinkComponent,
    BlockComponent,
    TabComponent,
    TabLayoutComponent,
    TabLinkComponent,
    PageHeaderComponent,
    PagingListSizeComponent,
    PaginationComponent,
    PagingTextComponent,
    PagingControlComponent,
    MailLinkComponent,
    ModalLayoutComponent,
    ModalComponent,
    ModalHeaderComponent,
    PanelComponent,
    CardComponent,
    CardGroupComponent,
    ProgressBarComponent,
    SpinnerComponent,
    SwitchComponent,
    RowComponent,
    ColComponent
  ],
  exports: [
    AlertComponent,
    AlertHeadingComponent,
    HelpBlockComponent,
    LayoutComponent,
    LoremIpsumComponent,
    SimpleLayoutComponent,
    ButtonComponent,
    ButtonGroupComponent,
    BsGroupComponent,
    NavBarComponent,
    NavBarNavComponent,
    NavBarNavItemComponent,
    NavBarNavDropdownItemComponent,
    BsSizeDirective,
    BsContextDirective,
    BsBgContextDirective,
    BsTextContextDirective,
    TextAlignDirective,
    WidthDirective,
    BorderDirective,
    DeprecatedDirective,
    SpinnerDirective,
    ScrollableDirective,
    DropdownComponent,
    DropdownMenuComponent,
    DropdownMenuItemComponent,
    DropdownDividerComponent,
    IconComponent,
    FigureComponent,
    InnerLinkComponent,
    OuterLinkComponent,
    BlockComponent,
    TabComponent,
    TabLayoutComponent,
    TabLinkComponent,
    PageHeaderComponent,
    PagingListSizeComponent,
    PaginationComponent,
    PagingControlComponent,
    MailLinkComponent,
    ModalComponent,
    ModalHeaderComponent,
    PanelComponent,
    CardComponent,
    CardGroupComponent,
    ProgressBarComponent,
    SpinnerComponent,
    SwitchComponent,
    RowComponent,
    ColComponent
  ],
  providers: [
    IconService
  ],
  entryComponents: [
    SpinnerComponent
  ]
})
export class UiModule {

}
