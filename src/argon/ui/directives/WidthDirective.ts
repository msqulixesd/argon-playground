import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { indexOf } from 'lodash';
import { LogService } from '../../core/services/LogService';

@Directive({
  selector: '[arWidth]'
})
export class WidthDirective {

  static WIDTH_PREFIX = 'w';

  static ALLOWED_VALUES = ['', '25', '50', '75', '100'];

  private currentWidth: number;

  private currentClass: string;

  @Input() set arWidth(width: number) {
    if (width === this.currentWidth) {
      return;
    }

    this.checkWidthValue(width);

    this.decorate(width);
  }

  constructor(
    protected renderer2: Renderer2,
    protected elementRef: ElementRef,
    private logService: LogService
  ) { }

  protected decorate(nextWidth: number) {
    this.renderer2.removeClass(this.elementRef.nativeElement, this.currentClass);
    this.currentWidth = nextWidth;
    this.currentClass = nextWidth ? `${WidthDirective.WIDTH_PREFIX}-${this.currentWidth}` : '';

    if (this.currentClass) {
      this.renderer2.addClass(this.elementRef.nativeElement, this.currentClass);
    }
  }

  protected checkWidthValue(value: any) {
    const stringValue = value ? String(value) : '';

    if (indexOf(WidthDirective.ALLOWED_VALUES, stringValue) < 0) {
      this.logService.warn(`WidthDirective: Only ${WidthDirective.ALLOWED_VALUES} values could be accepted`);
    }
  }
}
