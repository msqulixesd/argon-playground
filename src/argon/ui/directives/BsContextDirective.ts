import { Directive, ElementRef, Input, Optional, Renderer2, SkipSelf} from '@angular/core';
import { BsContextType } from '../types/BsContextEnumType';
import { ClassNameDirective } from './ClassNameDirective';
import { BsGroupComponent } from '../components/BsGroup/BsGroupComponent';

@Directive({
  selector: '[arBsContext]'
})
export class BsContextDirective extends ClassNameDirective<BsContextType, string> {

  protected prefix = '';

  @Input() set arBsContextPrefix(prefix: string) {
    this.prefix = prefix || '';
    this.decorate(this.originalContext);
  }
  get arBsContextPrefix(): string {
    return this.prefix;
  }

  @Input() set arBsContext(context: BsContextType) {
    let nextContext = context;
    if (!nextContext && this.bsContextGroup) {
      nextContext = this.bsContextGroup.bsContext as any;
      this.unsubscribeFromParent();
    } else if (!nextContext && this.parentDirective && this.parentDirective.arBsContext) {
      nextContext = this.parentDirective.arBsContext
        ? this.parentDirective.arBsContext
        : '' as BsContextType;
      this.subscribeToParent();
    } else {
      this.unsubscribeFromParent();
    }

    this.decorate(nextContext);
  }
  get arBsContext(): BsContextType {
    return this.originalContext;
  }

  constructor(
    renderer2: Renderer2,
    elementRef: ElementRef,
    @Optional() protected bsContextGroup: BsGroupComponent,
    @Optional() @SkipSelf() protected parentDirective: BsContextDirective
  ) {
    super(renderer2, elementRef, parentDirective);
  }

  protected createContextClassName(originalClass: string, contextValue: string): string {
    if (!originalClass) {
      return '';
    }

    return this.prefix
      ? `${originalClass}-${this.prefix}-${contextValue}`
      : `${originalClass}-${contextValue}`;
  }

  protected createEnumTypeInstance(context: BsContextType): string {
    return String(context);
  }

}
