import { first } from 'lodash';
import { ElementRef, OnDestroy, Renderer2 } from '@angular/core';
import { Subject ,  Subscription } from 'rxjs';

export abstract class ClassNameDirective<K, T> implements OnDestroy {

  public context: T;

  public originalContext: K;

  public change = new Subject<K>();

  protected parentSubscription: Subscription;

  protected originalClassName: string;

  protected contextClassName: string;

  public set baseClassName(val: string) {
    this.originalClassName = val;
    this.decorate(this.originalContext);
  }
  public get baseClassName(): string {
    return this.originalClassName;
  }

  constructor(
    protected renderer2: Renderer2,
    protected elementRef: ElementRef,
    protected parentDirective: ClassNameDirective<K, T>
  ) {
    const classList = this.elementRef.nativeElement.classList;
    this.originalClassName = classList ? first(classList) as string : '';
  }

  ngOnDestroy(): void {
    this.unsubscribeFromParent();
  }

  protected subscribeToParent() {
    if (!this.parentSubscription && this.parentDirective) {
      this.parentSubscription = this.parentDirective.change.subscribe(this.decorate);
    }
  }

  protected unsubscribeFromParent() {
    if (this.parentSubscription) {
      this.parentSubscription.unsubscribe();
      this.parentSubscription = null;
    }
  }

  protected decorate = (context: K): void => {
    this.change.next(context);
    this.context = this.createEnumTypeInstance(context);
    this.originalContext = context;
    const nextContextClassName = this.createContextClassName(this.originalClassName, this.context);

    if (String(this.contextClassName) === String(nextContextClassName)) {
      return;
    }

    if (this.contextClassName) {
      this.renderer2.removeClass(this.elementRef.nativeElement, this.contextClassName);
    }

    if (nextContextClassName) {
      this.contextClassName = nextContextClassName;
      this.renderer2.addClass(this.elementRef.nativeElement, this.contextClassName);
    }
  }

  protected abstract createContextClassName(originalClass: string, contextValue: T): string;

  protected abstract createEnumTypeInstance(context: K): T;

}
