import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[arScrollable]'
})
export class ScrollableDirective implements OnInit {

  static CLASS_NAME = 'ar-ui-scrollable-block';

  @Input() set arScrollable(height: any) {
    this.currentHeight = height || 'auto';

    if (this.elementRef.nativeElement) {
      this.renderer2.setStyle(this.elementRef.nativeElement, 'height', this.currentHeight);
    }
  }

  private currentHeight: any;

  constructor(
    protected renderer2: Renderer2,
    protected elementRef: ElementRef,
  ) { }

  ngOnInit(): void {
    this.renderer2.addClass(this.elementRef.nativeElement, ScrollableDirective.CLASS_NAME);
    this.renderer2.setStyle(this.elementRef.nativeElement, 'height', this.currentHeight);
    this.renderer2.setStyle(this.elementRef.nativeElement, 'display', 'block');
  }

}
