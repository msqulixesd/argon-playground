import { Injectable, TemplateRef, ViewContainerRef} from '@angular/core';
import { ModalLayoutViewModel } from '../components/ModalLayout/ModalLayoutViewModel';
import { ModalDialogConfigModel } from '../models/ModalDialogConfigModel';
import { LogService } from '../../core/services/LogService';

@Injectable()
export class ModalService {

  private layoutViewModel: ModalLayoutViewModel;

  constructor(
    protected logService: LogService
  ) { }

  public showModal(template: TemplateRef<any>, config: ModalDialogConfigModel, context?: any) {
    this.checkLayout();
    if (this.layoutViewModel.template) {
      this.layoutViewModel.container.clear();
    }
    this.layoutViewModel.viewRef = this.layoutViewModel.container.createEmbeddedView(template, { $implicit: context });
    this.layoutViewModel.template = template;
    this.layoutViewModel.config = config;
    this.layoutViewModel.opened = true;
  }

  public hideModal() {
    this.checkLayout();
    this.layoutViewModel.opened = false;
    this.layoutViewModel.template = null;
    this.layoutViewModel.viewRef = null;
    this.layoutViewModel.config = null;
    this.layoutViewModel.container.clear();
  }

  public registerLayout(viewContainer: ViewContainerRef): ModalLayoutViewModel {
    if (this.layoutViewModel) {
      this.logService.log('ModalService: skip Modal Layout already registered error');
    } else if (this.layoutViewModel) {
      throw new Error('ModalService: another instance Modal Layout already registered');
    }

    this.layoutViewModel = this.createViewModel(viewContainer);
    return this.layoutViewModel;
  }

  public unregisterLayout() {
    this.checkLayout();
    this.layoutViewModel = null;
  }

  private createViewModel(viewContainer: ViewContainerRef): ModalLayoutViewModel {
    return new ModalLayoutViewModel(viewContainer);
  }

  private checkLayout() {
    if (!this.layoutViewModel) {
      throw new Error('ModalService: modal layout not registered');
    }
  }
}
