import { find, without } from 'lodash';
import { TabModel } from '../models/TabModel';
import { TabInterface } from '../interfaces/TabInterface';

export abstract class AbstractTabService<T, K> {
  
  protected tabs: Array<T> = [];
  
  public abstract find(params: K): T;
  
  public abstract create(params: K): T;
  
  public add(model: T) {
    this.tabs.push(model);
  }
  
  public findOrCreate(params: K): T {
    let tab = this.find(params);
    if (!tab) {
      tab = this.create(params);
      this.add(tab);
    }
    
    return tab;
  }
  
  public remove(model: T) {
    this.tabs = without(this.tabs, model);
  }
  
  public findByTab(tab: TabModel): T {
    return find(this.tabs, ((item: TabInterface) => (tab === item.tab)) as any);
  }
  
  public removeByTab(tab: TabModel) {
    const model = this.findByTab(tab);
    this.remove(model);
  }
  
}
