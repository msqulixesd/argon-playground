import { Injectable, TemplateRef, ViewContainerRef, ViewRef } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { DisplayAlertMessage } from '../../core/messages/DisplayAlertMessage';

@Injectable()
export class AlertsAreaService {

  static MAX_ALERT_STACK_SIZE = 3;

  private container: ViewContainerRef;
  private template: TemplateRef<any>;

  public registerContainer(container: ViewContainerRef) {
    this.container = container;
  }

  public registerTemplate(template: TemplateRef<any>) {
    this.template = template;
  }

  public addAlert = (message: DisplayAlertMessage) => {
    this.removeOldestAlert();
    message.viewRef = this.container.createEmbeddedView(this.template, { $implicit: message });
    this.createAutohideObserver(message).subscribe(this.removeAlert);
  }

  public removeAlert = (message: DisplayAlertMessage) => {
    if (!message) {
      return;
    }
    this.removeAlertByViewRef(message.viewRef);
  }

  private removeOldestAlert() {
    if (this.container.length === AlertsAreaService.MAX_ALERT_STACK_SIZE) {
      this.container.remove(0);
    }
  }

  private removeAlertByViewRef(view: ViewRef) {
    const index = this.container.indexOf(view);
    if (index >= 0) {
      this.container.remove(index);
    }
  }

  private createAutohideObserver(message: DisplayAlertMessage): Observable<DisplayAlertMessage> {
    if (!message.shouldAutohide) {
      return of(null);
    }

    return of(message).pipe(delay(message.displayTime));
  }

}
