import { ViewRef } from '@angular/core';

export interface ScrollItemInterface {

  data: any;

  viewRef?: ViewRef;

  shouldDetach?: boolean;

}
