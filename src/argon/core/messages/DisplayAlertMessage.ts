import { MessageInterface } from '../interfaces/MessageInterface';
import { TemplateRef, ViewRef } from '@angular/core';

export class DisplayAlertMessage implements MessageInterface {

  static DEFAULT_DISPLAY_TIME = 5000;

  static type = 'DisplayAlertMessage';

  public type = DisplayAlertMessage.type;

  public viewRef: ViewRef;

  public template?: TemplateRef<any>;

  public templateContext: any;

  public get isSimple(): boolean { return !this.template; }

  public get shouldAutohide(): boolean { return this.displayTime > 0; }

  constructor(
    public message: string,
    public bsContext: string = 'info',
    public closeable: boolean = true,
    public displayTime: number = DisplayAlertMessage.DEFAULT_DISPLAY_TIME
  ) { }

  public isMessageType(messageClass: any): boolean {
    return messageClass.type === this.type;
  }

  public setTemplate(template: TemplateRef<any>, context?: any) {
    this.template = template;
    this.templateContext = context;
  }

}
