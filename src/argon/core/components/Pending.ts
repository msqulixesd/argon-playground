import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

export class Pending {

  pending: boolean;

  protected makeRequest(request: Observable<any>): Observable<any> {
    this.pending = true;
    return request.pipe(finalize(() => { this.pending = false; }));
  }

}
