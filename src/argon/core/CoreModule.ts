import { NgModule } from '@angular/core';
import { TrimPipe } from './pipes/TrimPipe';
import { MessageBusService } from './services/MessageBusService';
import { AlertService } from './services/AlertService';
import { TruncatePipe } from './pipes/TruncatePipe';
import { InfinityScrollDirective } from './directives/InfinityScrollDirective';
import { ScrollContainerDirective } from './directives/ScrollContainerDirective';
import { messageBusServiceFactory } from './factories/messageBusServiceFactory';
import { LogService } from './services/LogService';
import { DictionaryService } from './services/DictionaryService';
import { DictionaryPipe } from './pipes/DictionaryPipe';
import { DictionaryDirective } from './directives/DictionaryDirective';
import { dictionaryServiceFactory } from './factories/dictionaryServiceFactory';
import { QueryOptionsBuilderService } from './services/QueryOptionsBuilderService';

@NgModule({
  imports: [
  ],
  exports: [
    TrimPipe,
    TruncatePipe,
    DictionaryPipe,
    InfinityScrollDirective,
    ScrollContainerDirective,
    DictionaryDirective,
  ],
  declarations: [
    TrimPipe,
    TruncatePipe,
    DictionaryPipe,
    InfinityScrollDirective,
    ScrollContainerDirective,
    DictionaryDirective,
  ],
  providers: [
    { provide: MessageBusService, useFactory: messageBusServiceFactory, deps: [ LogService ] },
    { provide: DictionaryService, useFactory: dictionaryServiceFactory, deps: [ LogService ] },
    AlertService,
    LogService,
    QueryOptionsBuilderService,
  ]
})
export class CoreModule { }
