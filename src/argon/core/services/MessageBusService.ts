import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { MessageInterface } from '../interfaces/MessageInterface';
import { LogService } from './LogService';

@Injectable()
export class MessageBusService {

  private bus = new Subject<MessageInterface>();

  constructor(
    protected logService: LogService
  ) { }

  public of<T>(messageClass: any): Observable<T> {
    return this.bus.pipe(
      filter((message: MessageInterface) => message.isMessageType(messageClass)),
      map((message: any) => message as T)
    );
  }

  public send(message: MessageInterface) {
    this.logService.log('MessageBusService: send', message);
    this.bus.next(message);
  }
}
