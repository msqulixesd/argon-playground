import { Pipe, PipeTransform } from '@angular/core';
import { find } from 'lodash';
import { DictionaryService } from '../services/DictionaryService';
import { DictionaryItemInterface } from '../interfaces/DictionaryItemInterface';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'arDictionary'
})
export class DictionaryPipe implements PipeTransform {

  constructor(
    protected dictionaryService: DictionaryService
  ) { }

  transform(value: any, key: string): Observable<string> {
    return this.dictionaryService.get(key).pipe(
      map((dictionary: Array<DictionaryItemInterface>) => {
        const item = find(dictionary, { value });
        return item ? item.label : '';
      })
    );
  }
}
