import { Directive, Input, OnDestroy, TemplateRef, ViewContainerRef } from '@angular/core';
import { find } from 'lodash';
import { DictionaryService } from '../services/DictionaryService';
import { DictionaryItemInterface } from '../interfaces/DictionaryItemInterface';
import { Subscription } from 'rxjs/index';

@Directive({
  selector: '[arDictionary]'
})
export class DictionaryDirective implements OnDestroy {

  @Input() set arDictionaryName(value: string) {
    if (this._currentDictionaryKey === value) {
      return;
    }

    this.subscribeToDictionary(value);
  }

  @Input() set arDictionary(value: number) {
    if (value === this._currentValue) {
      return;
    }

    this._currentValue = value;
    this.updateView();
  }

  private _currentValue: number;

  private _currentDictionaryKey: string;

  private _currentDictionaryItem: DictionaryItemInterface;

  private _dictionary: Array<DictionaryItemInterface> = [];

  private _dictionarySubscription: Subscription;

  constructor(
    private container: ViewContainerRef,
    private template: TemplateRef<any>,
    private dictionaryService: DictionaryService
  ) { }

  ngOnDestroy(): void {
    this.unsubscribeFromDictionary();
  }

  private subscribeToDictionary(value: string) {
    this.unsubscribeFromDictionary();
    this._currentDictionaryKey = value;
    if (!this._currentDictionaryKey) {
      return;
    }

    this._dictionarySubscription = this.dictionaryService.get(this._currentDictionaryKey).subscribe(this.setDictionary);
  }

  private unsubscribeFromDictionary() {
    if (this._dictionarySubscription) {
      this._dictionarySubscription.unsubscribe();
    }
    this._dictionarySubscription = null;
  }

  private setDictionary = (dictionary: Array<DictionaryItemInterface>) => {
    this._dictionary = dictionary;
    this.updateView();
  }

  private updateView() {
    const nextItem: DictionaryItemInterface = this.getDictionaryItem();

    // TODO: Implement compare values function
    // if (nextItem && this._currentDictionaryItem && nextItem.value === this._currentDictionaryItem.value) {
    //   return;
    // }

    if (nextItem === this._currentDictionaryItem) {
      return;
    }

    this._currentDictionaryItem = nextItem;

    this.container.clear();

    if (nextItem) {
      this.container.createEmbeddedView(this.template, { $implicit: nextItem });
    }
  }

  private getDictionaryItem(): DictionaryItemInterface {
    return find(this._dictionary, (item: DictionaryItemInterface) => item.value === this._currentValue);
  }
}
