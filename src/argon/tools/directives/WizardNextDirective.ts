import { CdkStepper, CdkStepperNext } from '@angular/cdk/stepper';
import { Directive, HostListener } from '@angular/core';
import { Wizard } from '../components/Wizard/Wizard';

@Directive({
  selector: '[arWizardNext]',
  providers: [{provide: CdkStepper, useExisting: Wizard }]
})
export class WizardNextDirective extends CdkStepperNext {

  @HostListener('click') onClick() {
    this._stepper.next();
  }

}
