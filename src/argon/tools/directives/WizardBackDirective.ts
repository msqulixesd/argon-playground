import { CdkStepper, CdkStepperNext } from '@angular/cdk/stepper';
import { Directive, HostListener } from '@angular/core';
import { Wizard } from '../components/Wizard/Wizard';

@Directive({
  selector: '[arWizardBack]',
  providers: [{provide: CdkStepper, useExisting: Wizard }]
})
export class WizardBackDirective extends CdkStepperNext {

  @HostListener('click') onClick() {
    this._stepper.previous();
  }

}
