import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { WizardDirective } from '../../directives/WizardDirective';
import { Wizard } from './Wizard';
import { WizardAnimations } from './WizardAnimations';
import { StepperSelectionEvent } from '@angular/cdk/stepper';

@Component({
  selector: 'ar-wizard',
  templateUrl: 'WizardComponent.html',
  providers: [{provide: Wizard, useExisting: WizardComponent}],
  animations: [
    WizardAnimations.stepTransition
  ],
  styleUrls: ['WizardComponent.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WizardComponent extends WizardDirective {

  @Input() linear: boolean;

  @Input() selectedIndex: number;

  @Input() displayHeader = true;

  @Output() selectionChange: EventEmitter<StepperSelectionEvent>;

}
