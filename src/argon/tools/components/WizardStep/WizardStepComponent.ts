import { ChangeDetectionStrategy, Component, forwardRef, Inject, Input } from '@angular/core';
import { CdkStep } from '@angular/cdk/stepper';
import { Wizard } from '../Wizard/Wizard';
import { AbstractControl } from '@angular/forms';
import { FocusableOption } from '@angular/cdk/a11y';

@Component({
  selector: 'ar-wizard-step',
  template: `<ng-template><ng-content></ng-content></ng-template>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WizardStepComponent extends CdkStep implements FocusableOption {

  @Input() label: string;

  @Input() stepControl: AbstractControl;

  constructor(@Inject(forwardRef(() => Wizard)) stepper: Wizard) {
    super(stepper);
  }

  public focus() {

  }
}
