import { NgModule } from '@angular/core';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { CommonModule } from '@angular/common';
import { WizardComponent } from './components/Wizard/WizardComponent';
import { WizardStepComponent } from './components/WizardStep/WizardStepComponent';
import { WizardDirective } from './directives/WizardDirective';
import { UiModule } from '../ui/UiModule';
import { WizardHeaderComponent } from './components/WizardHeader/WizardHeaderComponent';
import { WizardNextDirective } from './directives/WizardNextDirective';
import { WizardBackDirective } from './directives/WizardBackDirective';
import { Wizard } from './components/Wizard/Wizard';

@NgModule({
  imports: [
    CommonModule,
    CdkStepperModule,
    UiModule,
  ],
  declarations: [
    Wizard,
    WizardComponent,
    WizardStepComponent,
    WizardDirective,
    WizardHeaderComponent,
    WizardNextDirective,
    WizardBackDirective,
  ],
  exports: [
    WizardComponent,
    WizardStepComponent,
    WizardNextDirective,
    WizardBackDirective,
  ]
})
export class ToolsModule { }
