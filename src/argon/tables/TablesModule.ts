import { NgModule } from '@angular/core';
import { TableComponent } from './components/Table/TableComponent';
import { TableOrderIconComponent } from './components/TableOrderIcon/TableOrderIconComponent';
import { UiModule } from '../ui/UiModule';
import { RowContextDirective } from './directives/RowContextDirective';
import { TableColPickerComponent } from './components/TableColPicker/TableColPickerComponent';
import { TableFilterComponent } from './components/TableFilter/TableFilterComponent';
import { CommonModule } from '@angular/common';
import { TableHeaderDirective } from './directives/TableHeaderDirective';
import { TableSwitchableCellDirective } from './directives/TableSwitchableCellDirective';
import { TableActionGroupComponent } from './components/TableActionGroup/TableActionGroupComponent';

@NgModule({
  imports: [
    CommonModule,
    UiModule,
  ],
  declarations: [
    RowContextDirective,
    TableHeaderDirective,
    TableSwitchableCellDirective,
    TableComponent,
    TableOrderIconComponent,
    TableColPickerComponent,
    TableFilterComponent,
    TableActionGroupComponent,
  ],
  exports: [
    RowContextDirective,
    TableHeaderDirective,
    TableSwitchableCellDirective,
    TableComponent,
    TableColPickerComponent,
    TableFilterComponent,
    TableActionGroupComponent
  ],
  providers: [

  ]
})
export class TablesModule { }
