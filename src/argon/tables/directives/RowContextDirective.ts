import { Directive, ElementRef, Input, Optional, Renderer2 } from '@angular/core';
import { BsContextType } from '../../ui/types/BsContextEnumType';
import { TableComponent } from '../components/Table/TableComponent';

@Directive({
  selector: '[arRowContext]'
})
export class RowContextDirective {

  static BG_PREFIX = 'bg';

  static TABLE_PREFIX = 'table';

  private currentContext: BsContextType;

  private currentBgClass: string;

  @Input() set arRowContext(context: BsContextType) {
    if (context !== this.currentContext) {
      const isDark = this.tableComponent && this.tableComponent.dark;
      this.decorate(context, isDark);
    }
  }

  constructor(
    protected renderer2: Renderer2,
    protected elementRef: ElementRef,
    @Optional() protected tableComponent: TableComponent,
  ) { }

  protected decorate(nextContext: BsContextType, isDark: boolean = false) {
    if (this.currentBgClass) {
      this.renderer2.removeClass(this.elementRef.nativeElement, this.currentBgClass);
    }

    this.currentContext = nextContext;
    this.currentBgClass = isDark
      ? `${RowContextDirective.BG_PREFIX}-${this.currentContext}`
      : `${RowContextDirective.TABLE_PREFIX}-${this.currentContext}`;

    this.renderer2.addClass(this.elementRef.nativeElement, this.currentBgClass);
  }
}
