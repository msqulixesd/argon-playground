import {
  Directive, ElementRef, EmbeddedViewRef, HostListener, Input, Renderer2,
  ViewContainerRef
} from '@angular/core';
import { TableComponent } from '../components/Table/TableComponent';

@Directive({
  selector: '[arTableHeader]'
})
export class TableHeaderDirective {

  static SWITCH_CONTAINER_CLASS = 'ar-table__switch';

  @Input() set arTableHeader(key: string) {
    this._key = key;
    this.toggleSortClass(this._key);
  }

  private _key: string;

  private _stateArrow: EmbeddedViewRef<any>;

  constructor(
    public table: TableComponent,
    private containerRef: ViewContainerRef,
    private elementRef: ElementRef,
    private renderer: Renderer2
  ) { }

  @HostListener('click') onMouseClick() {
    this.table.headerClick(this._key);
  }

  private toggleSortClass(key: string) {
    if (key && !this._stateArrow) {
      this.renderer.addClass(this.elementRef.nativeElement, TableHeaderDirective.SWITCH_CONTAINER_CLASS);
      this._stateArrow = this.containerRef.createEmbeddedView(this.table.sortArrowTemplate, { key });
    } else if (this._stateArrow) {
      const index = this.containerRef.indexOf(this._stateArrow);
      this.containerRef.remove(index);
      this.renderer.removeClass(this.elementRef.nativeElement, TableHeaderDirective.SWITCH_CONTAINER_CLASS);
      this._stateArrow = null;
    }
  }

}
