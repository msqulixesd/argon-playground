import {
  Directive, ElementRef, EmbeddedViewRef, Input, OnInit, Renderer2
} from '@angular/core';
import { TableComponent } from '../components/Table/TableComponent';
import { SwitchableCellInterface } from '../interfaces/SwitchableCellInterface';

@Directive({
  selector: '[arTableSwitchableCell]',
})
export class TableSwitchableCellDirective implements OnInit {

  static HIDDEN_ATTRIBUTE = 'hidden';

  @Input() set arTableSwitchableCell(key: string) {
    this._key = key;
  }

  @Input() set arTableSwitchableCellTitle(title: string) {
    this._title = title;
  }

  @Input() set arTableSwitchableCellHidden(hidden: boolean) {
    this._hidden = hidden;
  }

  private _key: string;

  private _title: string;

  private _hidden: boolean;

  private ref: EmbeddedViewRef<any>;

  constructor(
    public table: TableComponent,
    private renderer2: Renderer2,
    private elementRef: ElementRef
  ) { }

  ngOnInit(): void {
    this.table.registerSwitchableCell(this.getCell());
    this.table.onChangeCellsVisibility.subscribe(this.toggleCell);
    this.toggleCell();
  }

  private toggleCell = () => {
    if (this.table.isCellHidden(this._key)) {
      this.renderer2.setAttribute(this.elementRef.nativeElement, TableSwitchableCellDirective.HIDDEN_ATTRIBUTE, '');
    } else {
      this.renderer2.removeAttribute(this.elementRef.nativeElement, TableSwitchableCellDirective.HIDDEN_ATTRIBUTE);
    }
  }

  private getCell(): SwitchableCellInterface {
    return {
      key: this._key,
      title: this._title,
      hidden: this._hidden
    };
  }
}
