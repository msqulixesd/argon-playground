import {
  AfterViewInit, ChangeDetectionStrategy,
  Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild, ViewEncapsulation
} from '@angular/core';
import { Subject } from 'rxjs';
import { TableComponentInterface } from './TableComponentInterface';
import { TableSortOrderInterface } from '../../interfaces/TableSortOrderInterface';
import { SwitchableCellService } from './SwitchableCellService';
import { SwitchableCellInterface } from '../../interfaces/SwitchableCellInterface';
import { ContextualService } from '../../../ui/services/ContextualService';

@Component({
  selector: 'ar-table',
  template: `
    <table
      class="table ar-table"
      [class.table-striped]="striped"
      [class.table-bordered]="bordered"
      [class.table-hover]="hover"
      [class.table-responsive]="responsive" 
      [class.table-dark]="dark"
      [class.ar-table--scrollable]="scrollable"
      [arBsSize]="contextualService.bsSize"
      [arBsSizePrefix]="sizePrefix"
    >
      <ng-content select="[colgroup]"></ng-content>
      <thead class="thead" [arBsContext]="contextualService.bsContext">
        <tr>
          <td colspan="1000" class="table-controls-area">
            <ng-content select="[table-controls]"></ng-content>
          </td>
        </tr>
        <tr class="ar-table-header">
            <ng-content select="[header]"></ng-content>
        </tr>
      </thead>
      <ng-content></ng-content>
    </table>
    <ng-template #sortArrowTemplate let-key="key">
        <ar-table-order-icon [key]="key"></ar-table-order-icon>
    </ng-template>
  `,
  styleUrls: ['Table.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [SwitchableCellService, ContextualService],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableComponent implements OnInit, TableComponentInterface, AfterViewInit {

  static ASC = 'asc';

  static DESC = 'desc';

  @Input() defaultOrderBy: string;

  @Input() defaultOrderDirection: 'asc' | 'desc' = 'asc';

  @Input() striped: boolean;

  @Input() dark: boolean;

  @Input() bordered: boolean;

  @Input() hover: boolean;

  @Input() responsive: boolean;

  @Input() scrollable: boolean;

  @Output() order = new EventEmitter<TableSortOrderInterface>();

  @ViewChild('sortArrowTemplate') sortArrowTemplate: TemplateRef<any>;

  get sizePrefix(): string {
    return this.responsive ? 'responsive' : '';
  }

  public orderBy: string;

  public orderDirection: 'asc' | 'desc';

  public onChangeCellsVisibility = new Subject<void>();

  constructor(
    public switchableCellService: SwitchableCellService,
    public contextualService: ContextualService
  ) { }

  ngOnInit() {
    this.setOrder(this.defaultOrderBy, this.defaultOrderDirection);
  }

  ngAfterViewInit(): void {
    if (this.switchableCellService.hasCells) {
      this.onChangeCellsVisibility.next();
    }
  }

  public headerClick(key: string) {
    const nextOrderDirection = this.orderBy === key
      ? this.flipOrderDirection(this.orderDirection)
      : 'asc';
    this.setOrder(key, nextOrderDirection);
    this.order.emit(this.getTableOrder());
  }

  public setOrder(key: string, direction: 'asc' | 'desc' = 'asc') {
    this.orderBy = key;
    this.orderDirection = direction;
  }

  public isOrderKey(key: string): boolean {
    return key === this.orderBy;
  }

  public isAscDirection(): boolean {
    return this.orderDirection === 'asc';
  }

  public isDescDirection(): boolean {
    return this.orderDirection === 'desc';
  }

  // <editor-fold desc="[Switchable Cells]">
  public registerSwitchableCell(cell: SwitchableCellInterface) {
    this.switchableCellService.registerCell(cell);
  }

  public isCellHidden(key: string): boolean {
    return this.switchableCellService.isCellHidden(key);
  }

  public toggleCellVisibility(keys: Array<string>) {
    this.switchableCellService.toggleCellsVisibility(keys);
    this.onChangeCellsVisibility.next();
  }

  public toggleAllCellVisibility(isVisible: boolean) {
    this.switchableCellService.toggleAllCellsVisibility(isVisible);
    this.onChangeCellsVisibility.next();
  }
  // </editor-fold desc="[Switchable Cells]">

  private flipOrderDirection(direction: 'asc' | 'desc'): 'asc' | 'desc' {
    return direction === 'asc' ? 'desc' : 'asc';
  }

  private getTableOrder(): TableSortOrderInterface {
    return {
      orderBy: this.orderBy,
      orderDirection: this.orderDirection
    };
  }
}
