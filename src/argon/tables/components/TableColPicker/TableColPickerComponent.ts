import { Component } from '@angular/core';
import { SwitchableCellService } from '../Table/SwitchableCellService';
import { TableComponent } from '../Table/TableComponent';

@Component({
  selector: 'ar-table-col-picker',
  templateUrl: 'TableColPicker.component.html',
  styleUrls: ['TableColPicker.component.scss', '../Table/Table.component.scss']
})
export class TableColPickerComponent {

  constructor(
    public table: TableComponent,
    public service: SwitchableCellService
  ) { }

}
