import { Component, Input} from '@angular/core';
import { TableComponent } from '../Table/TableComponent';

@Component({
  selector: 'ar-table-order-icon',
  templateUrl: 'TableOrderIcon.component.html',
  styleUrls: ['TableOrderIcon.component.scss']
})
export class TableOrderIconComponent {

  @Input() key: string;

  constructor(
    public table: TableComponent
  ) { }

  getIconClass() {
    let className = '';

    if (this.table.isAscDirection()) {
      className += 'asc';
    }

    if (this.table.isOrderKey(this.key)) {
      className += ' active';
    }

    return className;
  }

}
