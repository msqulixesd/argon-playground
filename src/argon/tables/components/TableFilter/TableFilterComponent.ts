import { Component, ViewChild } from '@angular/core';
import { DropdownComponent } from '../../../ui/components/Dropdown/DropdownComponent';

@Component({
  selector: 'ar-table-filter',
  templateUrl: 'TableFilter.component.html',
  styleUrls: ['../Table/Table.component.scss'],
})
export class TableFilterComponent {

  @ViewChild('dropdown') dropdown: DropdownComponent;

  public close() {
    this.dropdown.close();
  }

}
