
export interface SwitchableCellInterface {
  
  key: string;
  
  title: string;
  
  hidden?: boolean;
  
}
