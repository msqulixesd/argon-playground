
export interface TableSortOrderInterface {

  orderBy: string;

  orderDirection: 'asc' | 'desc';

}
