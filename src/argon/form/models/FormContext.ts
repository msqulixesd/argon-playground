import { each } from 'lodash';
import { AbstractControl, FormGroup } from '@angular/forms';
import { createConfirmValidator } from '../factories/syncValidatorFactory';

export abstract class FormContext {

  constructor(
    public form: FormGroup
  ) { }

  public isInvalidControl(controlName: string): boolean {
    const control = this.form.get(controlName);

    if (!control) {
      return false;
    }

    return control.invalid && (control.dirty || control.touched);
  }

  public isValid(): boolean {
    this.touchAll();
    return this.form.valid;
  }

  public touchAll() {
    this.form.markAsTouched();
    each(this.form.controls, (control: AbstractControl) => {
      control.markAsTouched();
    });
  }

  public addFieldConfirmation(fieldName: string, confirmationName: string, message: string) {
    this.form.setValidators(createConfirmValidator(fieldName, confirmationName, message));
  }

}
