import { AutocompleteListComponent } from '../AutocompleteList/AutocompleteListComponent';
import { Component, Input, Optional, Renderer2, TemplateRef } from '@angular/core';
import { SelectOptionInterface } from '../../interfaces/SelectOptionInterface';
import { AbstractControl } from '@angular/forms';
import { InputGroupComponent } from '../InputGroup/InputGroupComponent';
import { FormService } from '../../services/FormService';
import { FormGroupComponent } from '../FormGroup/FormGroupComponent';
import { DictionaryService } from '../../../core/services/DictionaryService';

@Component({
  selector: 'ar-autocomplete-dictionary',
  templateUrl: '../Autocomplete/AutocompleteComponent.html',
  styleUrls: ['../Autocomplete/AutocompleteComponent.scss']
})
export class AutocompleteDictionaryComponent extends AutocompleteListComponent {

  @Input() placeholder = '';

  @Input() control: AbstractControl;

  @Input() name: string;

  @Input() noResultText = 'Nothing found';

  @Input() keyUpDelay = 500;

  @Input() set dictionary(key: string) {
    this._options = this.dictionaryService.get(key);
  }

  @Input() optionTemplate: TemplateRef<any>;

  @Input() defaultOption: SelectOptionInterface;

  @Input() dropUp: boolean;

  constructor(
    @Optional() formGroupComponent: FormGroupComponent,
    @Optional() formService: FormService,
    @Optional() inputGroup: InputGroupComponent,
    renderer: Renderer2,
    private dictionaryService: DictionaryService
  ) {
    super(formGroupComponent, formService, inputGroup, renderer);
  }

}
