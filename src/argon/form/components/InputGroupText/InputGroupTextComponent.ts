import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'ar-input-group-text',
  template: '<ng-content></ng-content>'
})
export class InputGroupTextComponent {

  @HostBinding('class.input-group-text') className = true;

}
