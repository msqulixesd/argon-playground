import { InputComponent } from '../Input/InputComponent';
import { map } from 'lodash';
import { Component, Input } from '@angular/core';
import { SelectOptionInterface } from '../../interfaces/SelectOptionInterface';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'ar-select',
  template: `
    <select
      class="custom-select"
      [arBsBgContext]="formService?.bsBgContext"
      [class.is-invalid]="hasError"
      arBsSize
      [formControl]="control"
      [name]="inputName"
      [size]="size"
    >
      <option [ngValue]="null">
        <ng-content></ng-content>
        {{ placeholder }}
      </option>
      <option *ngFor="let option of options" [ngValue]="option.value" [disabled]="option.disabled">
        {{ option.label || option.value }}
      </option>
    </select>
  `,
  styleUrls: ['../Input/InputComponent.scss']
})
export class SelectComponent extends InputComponent {

  @Input() options: Array<SelectOptionInterface> = [];

  @Input() public set list(val: Array<string>) {
    this.options = this.listToOptions(val);
  }

  @Input() placeholder = '';

  @Input() control: AbstractControl;

  @Input() name: string;

  @Input() size = 1;

  public listToOptions(list: Array<string> = []): Array<SelectOptionInterface> {
    return map(list, (item: string): SelectOptionInterface => ({
      label: item,
      value: item
    }));
  }
}
