import { CheckboxComponent } from '../Checkbox/CheckboxComponent';
import { Component, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'ar-radio',
  template: `
    <div class="custom-radio form-check"
         [class.custom-control-inline]="inline"
         *ngIf="control"
         arBsSize
         arBsContext
    >
      <input class="custom-control-input" [class.is-invalid]="hasError"
             type="radio" [id]="controlId" [formControl]="control" [value]="value" [name]="inputName">
      <label class="custom-control-label" [for]="controlId">
        <ng-content></ng-content>
      </label>
    </div>
  `,
  styleUrls: ['../Checkbox/CheckboxComponent.scss']
})
export class RadioComponent extends CheckboxComponent {

  @Input() value: any;

  @Input() inline: boolean;

  @Input() control: AbstractControl;

  @Input() name: string;

}
