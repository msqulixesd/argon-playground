import { SelectComponent } from '../Select/SelectComponent';
import { Component, Input, OnInit } from '@angular/core';
import { SelectOptionInterface } from '../../interfaces/SelectOptionInterface';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'ar-radio-group',
  template: `    
    <ng-container *ngIf="control">
      <div
        *ngFor="let option of options"
        class="custom-radio form-check"
        [class.custom-control-inline]="inline"
        arBsSize
        arBsContext
      >
        <input class="custom-control-input"
               [class.is-invalid]="hasError"
               type="radio" [id]="getRadioId(option.value)"
               [formControl]="control"
               [value]="option.value"
               [name]="inputName"
               [attr.disabled]="option.disabled"
        >
        <label class="custom-control-label" [for]="getRadioId(option.value)">
          {{ option.label }}
        </label>
      </div>
    </ng-container>
  `,
  styleUrls: ['../Checkbox/CheckboxComponent.scss']
})
export class RadioGroupComponent extends SelectComponent implements OnInit {

  @Input() options: Array<SelectOptionInterface> = [];

  @Input() inline: boolean;

  @Input() control: AbstractControl;

  @Input() name: string;

  public getRadioId(value: any): string {
    return this.formService
      ? this.formService.generateSubControlId(this.controlId, value)
      : '';
  }

}
