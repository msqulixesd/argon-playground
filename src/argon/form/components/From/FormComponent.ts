import { Component, HostBinding, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { FormService } from '../../services/FormService';
import { LogService } from '../../../core/services/LogService';

@Component({
  selector: 'ar-form, [arForm]',
  template: `
    <ng-content></ng-content>
  `,
  styleUrls: ['FormComponent.scss'],
  providers: [FormService],
  encapsulation: ViewEncapsulation.None
})
export class FormComponent implements OnInit {

  @Input() set inline(val: boolean) {
    this.isInline = Boolean(val);
  }

  @Input() id: string;

  @Input() set bgContext(val: string) {
    this.formService.bsBgContext = val;
  }

  @HostBinding('class.form-inline') isInline = false;
  @HostBinding('attr.autocomplete') autocomplete = 'off';

  constructor(
    private formService: FormService,
    private logService: LogService
  ) { }

  ngOnInit(): void {
    if (!this.id) {
      this.logService.warn('Form id param required!');
    }
    this.formService.formId = this.id;
  }

}
