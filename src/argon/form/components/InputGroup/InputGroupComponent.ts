import { Component, Input } from '@angular/core';

@Component({
  selector: 'ar-input-group',
  template: `
    <div class="input-group" arBsSize>
      <div class="input-group-prepend" *ngIf="prepend">
        <ng-content select="[prepend]"></ng-content>
      </div>
      <ng-content></ng-content>
      <div class="input-group-append" *ngIf="append">
        <ng-content select="[append]"></ng-content>
      </div>
    </div>
  `
})
export class InputGroupComponent {

  @Input() prepend: boolean;

  @Input() append: boolean;

}
