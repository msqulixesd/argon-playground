import { Component, Input} from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { SelectComponent } from '../Select/SelectComponent';

@Component({
  selector: 'ar-yes-no-select',
  template: `
    <select
      class="custom-select"
      [arBsBgContext]="formService?.bsBgContext"
      [class.is-invalid]="hasError"
      arBsSize
      [formControl]="control"
      [size]="size"
      [name]="inputName"
    >
      <option [ngValue]="null">{{ placeholder }}</option>
      <option [ngValue]="true">{{ yesLabel }}</option>
      <option [ngValue]="false">{{ noLabel }}</option>
    </select>
  `,
  styleUrls: ['../Input/InputComponent.scss']
})
export class YesNoSelectComponent extends SelectComponent {

  @Input() control: AbstractControl;

  @Input() placeholder = '';

  @Input() size = 1;

  @Input() name: string;

  @Input() yesLabel = 'Yes';

  @Input() noLabel = 'No';

}
