import { NgModule } from '@angular/core';
import { UiModule } from '../ui/UiModule';
import { CommonModule } from '@angular/common';
import { FormComponent } from './components/From/FormComponent';
import { FormGroupComponent } from './components/FormGroup/FormGroupComponent';
import { InputComponent } from './components/Input/InputComponent';
import { ReactiveFormsModule } from '@angular/forms';
import { ErrorMessageService } from './services/ErrorMessageService';
import { CheckboxComponent } from './components/Checkbox/CheckboxComponent';
import { RadioComponent } from './components/Radio/RadioComponent';
import { SelectComponent } from './components/Select/SelectComponent';
import { YesNoSelectComponent } from './components/YesNoSelect/YesNoSelectComponent';
import { SelectMultipleComponent } from './components/SelectMultiple/SelectMultipleComponent';
import { RadioGroupComponent } from './components/RadioGroup/RadioGroupComponent';
import { FileComponent } from './components/File/FileComponent';
import { InputGroupComponent } from './components/InputGroup/InputGroupComponent';
import { InputGroupTextComponent } from './components/InputGroupText/InputGroupTextComponent';
import { TextareaComponent } from './components/Textarea/TextareaComponent';
import { AutocompleteComponent } from './components/Autocomplete/AutocompleteComponent';
import { AutocompleteOptionComponent } from './components/Autocomplete/AutocompleteOptionComponent';
import { CoreModule } from '../core/CoreModule';
import { AutocompleteListComponent } from './components/AutocompleteList/AutocompleteListComponent';
import { AutocompleteDictionaryComponent } from './components/AutocompleteDictionary/AutocompleteDictionaryComponent';

@NgModule({
  imports: [
    CommonModule,
    UiModule,
    CoreModule,
    ReactiveFormsModule
  ],
  declarations: [
    FormComponent,
    FormGroupComponent,
    InputComponent,
    CheckboxComponent,
    RadioComponent,
    SelectComponent,
    YesNoSelectComponent,
    SelectMultipleComponent,
    RadioGroupComponent,
    FileComponent,
    InputGroupComponent,
    InputGroupTextComponent,
    TextareaComponent,
    AutocompleteComponent,
    AutocompleteOptionComponent,
    AutocompleteListComponent,
    AutocompleteDictionaryComponent,
  ],
  exports: [
    FormComponent,
    FormGroupComponent,
    InputComponent,
    CheckboxComponent,
    RadioComponent,
    SelectComponent,
    YesNoSelectComponent,
    SelectMultipleComponent,
    RadioGroupComponent,
    FileComponent,
    InputGroupComponent,
    InputGroupTextComponent,
    TextareaComponent,
    AutocompleteComponent,
    AutocompleteListComponent,
    AutocompleteDictionaryComponent,
  ],
  providers: [
    ErrorMessageService
  ]
})
export class FormModule { }
