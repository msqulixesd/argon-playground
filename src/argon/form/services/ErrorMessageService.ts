import { first, toPairs, last, has, get, map } from 'lodash';
import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Injectable()
export class ErrorMessageService {

  static getError(key: string, options: any): string {
    switch (key) {
      case 'required': return 'Value is required';
      case 'maxlength': return `Value length should be less or equal ${options.requiredLength}`;
      case 'minlength': return `Value length should be greater than or equal to ${options.requiredLength}`;
      case 'email': return 'The email must be a valid email address.';
      case 'asyncError': return options;
      case 'size': return `Value length should be equal ${options.requiredSize}`;
      case 'min': return `Value length should be greater than or equal to ${options.min}`;
      case 'max': return `Value should be less or equal ${options.max}`;
      case 'confirmation': return options || 'Fields are mot equals';
      case 'alpha_dash': return options || 'Fields may only contain letters, numbers and dashes.';
      case 'serverError': return options;
      default: return 'Unknown Error';
    }
  }

  public getErrorMessageList(control: AbstractControl, customMessages?: { [key: string]: string }): Array<string> {
    return map(toPairs(control.errors), (error) => this.getErrorMessage(error, customMessages));
  }

  public getFirstErrorMessage(control: AbstractControl, customMessages?: { [key: string]: string }): string {
    return this.getErrorMessage(first(toPairs(control.errors)), customMessages);
  }

  private getErrorMessage(error: Array<any>, customMessages: { [key: string]: string } = {}): string {
    const key = first(error);
    const options = last(error);

    if (has(customMessages, key)) {
      return get(customMessages, key) as string;
    }

    return ErrorMessageService.getError(key, options);
  }

}
